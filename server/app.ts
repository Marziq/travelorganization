import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as session from 'express-session';
import * as mongoose from 'mongoose';
import * as userServer from './models/user-server';
import * as travelServer from './models/travel-server';
import * as galleryServer from './models/gallery-server';

const config = require('./config/config');
const app = express();

app.use(bodyParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(
  session({
    secret: 'travel Organization',
    resave: false,
    saveUninitalized: true
  })
);
app.engine('html', require('ejs').renderFile);

// Check if user is login
app.use((req, res, next) => {
  if (
    req.url !== '/api/user/login' &&
    req.url !== '/api/user/registration' &&
    (!req.session.user ||
      typeof req.session.user === 'undefined' ||
      !req.session.user.email)
  ) {
    res.status(401).json({
      status: false,
      message: 'Sesja użytkownika wygasła.'
    });
  } else {
    mongoose.connect(
      process.env.DATABASE_URL + process.env.DATABASE_NAME,
      { useNewUrlParser: true }
    );
    next();
  }
});

app.use('/api/user', userServer);
app.use('/api/travels', travelServer);
app.use('/api/images', galleryServer);

const server = app.listen(8000, 'localhost', () => {
  const { address, port } = server.address();
  console.log('Nasłuchiwanie na adresie %s', address);
});
