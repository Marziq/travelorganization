"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require('mongoose');
var TravelSchema = mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    dateFrom: {
        type: String,
        required: true
    },
    dateTo: {
        type: String,
        required: false
    },
    place: {
        name: {
            type: String,
            required: false
        },
        address: {
            type: String,
            required: false
        }
    },
    description: {
        type: String,
        required: false
    },
    price: {
        type: Number,
        required: false
    },
    thingsToPack: [
        {
            name: {
                type: String,
                required: true
            },
            taken: {
                type: Boolean,
                required: false
            }
        }
    ],
    transport: [
        {
            type: {
                type: String,
                required: true
            },
            dateFrom: {
                type: String,
                required: false
            },
            dateTo: {
                type: String,
                required: false
            },
            placeFrom: {
                name: {
                    type: String,
                    required: false
                },
                address: {
                    type: String,
                    required: false
                }
            },
            placeTo: {
                name: {
                    type: String,
                    required: false
                },
                address: {
                    type: String,
                    required: false
                }
            },
            price: {
                type: Number,
                required: false
            }
        }
    ],
    accommodations: [
        {
            dateFrom: {
                type: String,
                required: false
            },
            dateTo: {
                type: String,
                required: false
            },
            place: {
                name: {
                    type: String,
                    required: false
                },
                address: {
                    type: String,
                    required: false
                }
            },
            price: {
                type: Number,
                required: false
            }
        }
    ],
    attractions: [
        {
            name: {
                type: String,
                required: true
            },
            date: {
                type: String,
                required: false
            },
            place: {
                name: {
                    type: String,
                    required: false
                },
                address: {
                    type: String,
                    required: false
                }
            },
            price: {
                type: Number,
                required: false
            }
        }
    ],
    food: [
        {
            name: {
                type: String,
                required: true
            },
            date: {
                type: String,
                required: false
            },
            price: {
                type: Number,
                required: false
            }
        }
    ],
    images: [{ name: String }]
});
module.exports = mongoose.model('Travels', TravelSchema, 'travels');
//# sourceMappingURL=travel.js.map