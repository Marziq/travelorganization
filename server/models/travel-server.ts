import * as express from 'express';
import * as mongoose from 'mongoose';

const User = require('../schemas/user');
const Travel = require('../schemas/travel');
const router = express.Router();

router.use((req, res, next) => {
  if (req.params.id) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      res.status(400).json({
        message: 'Niepoprawny ID wycieczki.'
      });
      return;
    }
  }
  next();
});

router.get('/', (req, res) => {
  User.findOne({ email: req.session.user.email }, (err, user) => {
    if (err) {
      console.error(err);
    }
    if (!user) {
      res.status(404).json({
        message: 'Użytkownik nie posiada żadnych wycieczek.'
      });
    } else {
      res.status(200).json({
        travels: user.travels
      });
    }
  })
    .select('travels')
    .populate('travels');
});

router.get('/:id', async (req, res) => {
  const travelID = req.params.id;
  User.findOne(
    {
      email: req.session.user.email,
      travels: travelID
    },
    { 'travels.$': 1 },
    (err, user) => {
      if (!user) {
        res.status(404).json({
          message: 'Wycieczka o podanym ID nie istnieje.'
        });
      } else {
        res.status(200).json({
          travel: user.travels
        });
      }
    }
  )
    .select('travels')
    .populate('travels');
});

router.post('/', async (req, res) => {
  const travel = new Travel(req.body);
  travel.save(err => {
    if (err) {
      console.error(err);
      res.status(404).json({
        message: 'Problem z dodaniem wycieczki'
      });
    } else {
      User.findOne({ email: req.session.user.email }, (err, user) => {
        if (!user) {
          res.status(404).json({
            message: 'Nie znaleziono użytkownika do dodania wycieczki'
          });
        } else {
          user.travels.push(travel);
          user.save(err => {
            if (err) {
              console.error(err);
              res.status(404).json({
                message: 'Wycieczka została dodana'
              });
            } else {
              res.status(200).json({
                message: 'Wycieczka została dodana',
                travelID: travel._id
              });
            }
          });
        }
      });
    }
  });
});

router.delete('/:id', function(req, res) {
  const travelID = req.params.id;
  Travel.findOneAndDelete({ _id: travelID }, (err, doc) => {
    if (err) {
      console.error(err);
      res.status(404).json({
        message: 'Błąd przy usuwaniu wycieczki. Spróbuj ponownie później.'
      });
    } else {
      User.updateOne(
        { email: req.session.user.email },
        { $pull: { travels: travelID } },
        (errUser, val) => {
          if (errUser) {
            console.error(errUser);
            res.status(503).json({
              message: 'Błąd przy usuwaniu wycieczki. Spróbuj ponownie później.'
            });
          } else {
            res.status(200).json({
              message: 'Wycieczka została poprawnie usunięta.'
            });
          }
        }
      );
    }
  });
});

router.put('/:id', (req, res) => {
  const travelID = req.params.id;
  const data = req.body.travel;
  Travel.findOne({ _id: travelID }, (err, doc) => {
    if (err) {
      console.error(err);
      res.status(503).json({
        message: 'Wystąpił problem przy edycji danych. Spróbuj ponownie później'
      });
    } else {
      doc.set({
        title: data._title ? data._title : '',
        dateFrom: data._dateFrom ? data._dateFrom : null,
        dateTo: data._dateTo ? data._dateTo : null,
        place: data._place ? data._place : null,
        description: data._description ? data._description : '',
        price: data._price ? data._price : 0
      });
      doc.save(err => {
        if (err) {
          console.error(err);
          res.status(503).json({
            message: 'Problem z aktualizowaniem wycieczki.'
          });
        } else {
          res.status(200).json({
            message: 'Wycieczka została zaktualizowana.'
          });
        }
      });
    }
  });
});

router.put('/:id/food', (req, res) => {
  const travelID = req.params.id;
  const data = req.body;
  Travel.findOne({ _id: travelID }, (err, doc) => {
    if (err) {
      console.error(err);
      res.status(503).json({
        message: 'Wystąpił problem przy edycji danych. Spróbuj ponownie później'
      });
    } else {
      doc.food = data.food;
      doc.save(err => {
        if (err) {
          console.error(err);
          res.status(503).json({
            message: 'Problem z aktualizowaniem wycieczki.'
          });
        } else {
          res.status(200).json({
            message: 'Jedzenie zostało zaktualizowane.'
          });
        }
      });
    }
  });
});

router.put('/:id/thingstopack', (req, res) => {
  const travelID = req.params.id;
  const data = req.body;
  Travel.findOne({ _id: travelID }, (err, doc) => {
    if (err) {
      console.error(err);
      res.status(503).json({
        message: 'Wystąpił problem przy edycji danych. Spróbuj ponownie później'
      });
    } else {
      doc.thingsToPack = data.thingsToPack;
      doc.save(err => {
        if (err) {
          console.error(err);
          res.status(503).json({
            message: 'Problem z aktualizowaniem wycieczki.'
          });
        } else {
          res.status(200).json({
            message: 'Rzeczy do zabrania zostały zaktualizowane.'
          });
        }
      });
    }
  });
});

router.put('/:id/attractions', async (req, res) => {
  const travelID = req.params.id;
  const data = req.body;
  Travel.findOne({ _id: travelID }, (err, doc) => {
    if (err) {
      console.error(err);
      res.status(503).json({
        message: 'Wystąpił problem przy edycji danych. Spróbuj ponownie później'
      });
    } else {
      doc.attractions = data.attractions;
      doc.save(err => {
        if (err) {
          console.error(err);
          res.status(503).json({
            message: 'Problem z aktualizowaniem wycieczki.'
          });
        } else {
          res.status(200).json({
            message: 'Atrakcje zostały zaktualizowane.'
          });
        }
      });
    }
  });
});

router.put('/:id/transport', (req, res) => {
  const travelID = req.params.id;
  const data = req.body;
  Travel.findOne({ _id: travelID }, (err, doc) => {
    if (err) {
      console.error(err);
      res.status(503).json({
        message: 'Wystąpił problem przy edycji danych. Spróbuj ponownie później'
      });
    } else {
      doc.transport = data.transport;
      doc.save(err => {
        if (err) {
          console.error(err);
          res.status(503).json({
            message: 'Problem z aktualizowaniem wycieczki.'
          });
        } else {
          res.status(200).json({
            message: 'Transport zostały zaktualizowany.'
          });
        }
      });
    }
  });
});

router.put('/:id/accommodations', (req, res) => {
  const travelID = req.params.id;
  const data = req.body;
  Travel.findOne({ _id: travelID }, (err, doc) => {
    if (err) {
      console.error(err);
      res.status(503).json({
        message: 'Wystąpił problem przy edycji danych. Spróbuj ponownie później'
      });
    } else {
      doc.accommodations = data.accommodations;
      doc.save(err => {
        if (err) {
          console.error(err);
          res.status(503).json({
            message: 'Problem z aktualizowaniem wycieczki.'
          });
        } else {
          res.status(200).json({
            message: 'Nocleg zostały zaktualizowany.'
          });
        }
      });
    }
  });
});

module.exports = router;
