import * as express from 'express';
import * as cryptoName from 'crypto';
import * as path from 'path';
import * as multer from 'multer';
import * as GridFsStorage from 'multer-gridfs-storage';
import * as Grid from 'gridfs-stream';
import * as mongoose from 'mongoose';

const Travel = require('../schemas/travel');
const router = express.Router();
let url = 'mongodb://localhost:27017/';
let databaseName = 'travelOrganization';

let gfs;
let conn = mongoose.createConnection(url + databaseName);

conn.once('open', function() {
  // init stream
  gfs = Grid(conn.db, mongoose.mongo);
  gfs.collection('gallery');
});

router.use((req, res, next) => {
  if (req.params.id) {
    if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
      res.status(400).json({
        message: 'Niepoprawny ID wycieczki.'
      });
      return;
    }
  }
  next();
});

// Create storage engine

const storage = new GridFsStorage({
  url: url + databaseName,
  file: (req, file) => {
    return new Promise((resolve, reject) => {
      cryptoName.randomBytes(16, (err, buf) => {
        if (err) {
          return reject(err);
        }
        const filename = buf.toString('hex') + path.extname(file.originalname);
        const fileInfo = {
          filename: filename,
          bucketName: 'gallery'
        };
        resolve(fileInfo);
      });
    });
  }
});
const upload = multer({ storage });

// API gallery

router.get('/', (req, res) => {
  gfs.files.find().toArray((err, files) => {
    if (!files || files.length === 0) {
      res.status(200).json({ files: false });
    } else {
      files.map(file => {
        if (
          file.contentType === 'image/jpeg' ||
          file.contentType === 'image/png'
        ) {
          file.isImage = true;
        } else {
          file.isImage = false;
        }
      });
      res.status(200).json({ files: files });
    }
  });
});

router.get('/:imagename', (req, res) => {
  const imageName = req.params.imagename;
  gfs.files.find({ filename: imageName }).toArray(function(err, files) {
    if (err) {
      res.status(404).json(err);
    }
    if (files.length > 0) {
      const readStream = gfs.createReadStream({ filename: imageName });
      readStream.pipe(res);
    } else {
      res.status(404).json({ status: false, message: 'Nie znaleziono pliku' });
    }
  });
});

router.get('/travel/:id', (req, res) => {
  const travelID = req.params.id;
  Travel.findOne(
    {
      _id: travelID
    },
    (err, travel) => {
      if (err) {
        console.error(err);
      }
      if (!travel) {
        res.status(404).json({
          message: 'Wycieczka o podanym ID nie istnieje.'
        });
      } else {
        if (travel.images && travel.images.length > 0) {
          const images = travel.images.map(item => {
            return item.name;
          });
          gfs.files
            .find({ filename: { $in: images } })
            .toArray((err, files) => {
              if (!files || files.length === 0) {
                res.json({ files: [] });
              } else {
                files.map(file => {
                  if (
                    file.contentType === 'image/jpeg' ||
                    file.contentType === 'image/png'
                  ) {
                    file.isImage = true;
                  } else {
                    file.isImage = false;
                  }
                });
                res.json({ files: files });
              }
            });
        } else {
          res.json({ files: [] });
        }
      }
    }
  );
});

router.delete('/:imagename/travel/:id', (req, res) => {
  const travelID = req.params.id;
  const imageName = req.params.imagename;
  Travel.findOne(
    {
      _id: travelID
    },
    (err, travel) => {
      if (err) {
        console.error(err);
      }
      if (!travel) {
        res.status(404).json({
          message: 'Wycieczka o podanym ID nie istnieje.'
        });
      } else {
        if (imageName) {
          gfs.remove(
            { filename: imageName, root: 'gallery' },
            (err, gridStore) => {
              if (err) {
                res.status(404).json({
                  message: 'Nie można usunąć wycieczki.'
                });
              } else {
                travel.images.forEach((image, i) => {
                  if (image.name === imageName) {
                    travel.images.splice(i, 1);
                  }
                });
                travel.save();
                res.status(200).json({
                  message: 'Zdjęcie zostało usunięte.'
                });
              }
            }
          );
        } else {
          res.status(400).json({
            message: 'Niepoprawna nazwa zdjęcia.'
          });
        }
      }
    }
  );
});

router.post('/travel/:id', upload.single('image'), async function(req, res) {
  const travelID = req.params.id;
  Travel.findOne(
    {
      _id: travelID
    },
    (err, travel) => {
      if (err) {
        console.error(err);
      }
      if (!travel) {
        res.statusCode = 404;
        res.json({
          message: 'Wycieczka o podanym ID nie istnieje.'
        });
      } else {
        travel.images.push({ name: res.req.file.filename });
        travel.save();
        res.status(200).json({
          message: 'Dodano zdjęcie do wycieczki',
          file: req.file
        });
      }
    }
  );
});

module.exports = router;
