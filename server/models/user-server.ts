import * as express from 'express';
const bcrypt = require('bcrypt');
let User = require('../schemas/user');
let router = express.Router();

router.get('/', (req, res) => {
  User.findOne({ email: req.session.user.email }, (err, user) => {
    if (!user) {
      res.status(404).json({
        message: 'Użytkownik o podanym adresie email nie istnieje.'
      });
    } else {
      res.status(200).json({
        user: user
      });
    }
  })
    .select('firstName lastName email travels')
    .populate('travels');
});

router.post('/login', function(req, res) {
  const { email, password } = req.body;
  if (email && password) {
    User.findOne({ email }, (err, user) => {
      if (err) {
        console.error(err);
      }
      if (!user) {
        res.status(404).json({
          status: false,
          message: 'Użytkownik o podanym adresie email nie istnieje!'
        });
      } else {
        if (bcrypt.compareSync(password, user.password)) {
          req.session.user = user;
          req.session.save();
          res.status(200).json({
            message: 'Użytkownik został poprawnie zalogowany'
          });
        } else {
          res.status(401).json({
            message: 'Nieprawidłowy email lub hasło.'
          });
        }
      }
    }).select('+password');
  } else {
    res.status(401).json({
      message: 'Nieprawidłowy email lub hasło.'
    });
  }
});

router.post('/registration', function(req, res) {
  const newUser = new User({
    email: req.body.email,
    password: req.body.password,
    firstName: req.body.firstName,
    lastName: req.body.lastName
  });
  var hash = bcrypt.hashSync(newUser.password, 10);
  newUser.password = hash;
  newUser.save(err => {
    if (err) {
      res.status(500).json({
        message:
          err.code === 11000
            ? 'Użytkownik o podanym adresie email istnieje już w bazie'
            : 'Wystąpił problem z utworzeniem nowego konta użytkownika.'
      });
    } else {
      res.status(200).json({
        message: 'Nowe konto zostało utworzone.'
      });
    }
  });
});

router.get('/isloggedin', (req, res) => {
  res.json({
    status: !!req.session.user
  });
});

router.get('/logout', (req, res) => {
  req.session.user = undefined;
  req.session.save();
  res.json({
    status: !req.session.user
  });
});

module.exports = router;
