var MongoClient = require('mongodb').MongoClient;
var url = 'mongodb://localhost:27017/';

MongoClient.connect(
  url,
  { useNewUrlParser: true },
  function(err, db) {
    if (err) throw err;
    var dbo = db.db('travelOrganization');

    dbo.collection('travels').drop(function(err, delOK) {
      if (err) throw err;
      if (delOK) console.log('Collection deleted');
      db.close();
    });

    var travels = [
      {
        title: 'Kurs wspinaczki skałkowej',
        dateFrom: new Date('2018-4-28').toISOString(),
        dateTo: new Date('2018-5-3').toISOString(),
        place: { name: 'Jura Krakowsko-Częstochowska' },
        description:
          'Piękna majówka podczas której wspinaliśmy się codziennie w skałkach.',
        price: 1500.0,
        thingsToPack: [
          { name: 'Śpiwór' },
          { name: 'Plecak' },
          { name: 'Ciuchy do wspinaczki' }
        ],
        transport: [
          {
            type: 'Pociąg',
            dateFrom: new Date('2018-4-27 15:20').toISOString(),
            dateTo: new Date('2018-4-27 20:25').toISOString(),
            placeFrom: { name: 'Wrocław Główny' },
            placeTo: { name: 'Kraków Główny' },
            price: 26.5
          }
        ]
      },
      {
        title: 'Pyrkon',
        dateFrom: new Date('2018-4-18').toISOString(),
        dateTo: new Date('2018-4-21').toISOString(),
        place: { name: 'Poznań' },
        description: 'Było genialnie! ♥',
        price: 600.0,
        thingsToPack: [
          { name: 'Śpiwór' },
          { name: 'Plecak' },
          { name: 'Ciuchy do stroju wikinga' }
        ],
        transport: [
          {
            type: 'Pociąg',
            dateFrom: new Date('2018-5-18 7:20').toISOString(),
            dateArrival: new Date('2018-5-18 10:25').toISOString(),
            placeFrom: { name: 'Wrocław Główny' },
            placeTo: { name: 'Poznań Główny' },
            price: 22.5
          }
        ]
      },
      {
        title: 'Wycieczka na weekend w Karkonosze',
        dateFrom: new Date('2018-3-23').toISOString(),
        dateTo: new Date('2018-3-5').toISOString(),
        place: { name: 'Karkonosze' },
        description: 'Wyjazd z bartkiem i Tomkiem.',
        price: 250.25,
        thingsToPack: [
          { name: 'Śpiwór' },
          { name: 'Plecak' },
          { name: 'Ciepłe ciuszki' }
        ],
        transport: [
          {
            type: 'Pociąg',
            dateFrom: new Date('2018-3-23 18:20').toISOString(),
            dateArrival: new Date('2018-3-23 21:25').toISOString(),
            placeFrom: { name: 'Wrocław Główny' },
            placeTo: { name: 'Karpacz' },
            price: 21.2
          }
        ]
      },
      {
        title: 'Wycieczka do Doliny Chochołowskiej',
        dateFrom: new Date('2017-12-1').toISOString(),
        dateTo: new Date('2017-12-4').toISOString(),
        place: { name: 'Dolina Chochołowska' },
        description: 'Weszliśmy na trzydniowiański wierch.',
        price: 200.0,
        thingsToPack: [
          { name: 'Śpiwór' },
          { name: 'Plecak' },
          { name: 'Raki' }
        ],
        transport: [
          {
            type: 'Autobus',
            dateFrom: new Date('2017-12-1 23:20').toISOString(),
            dateArrival: new Date('2017-12-2 6:25').toISOString(),
            placeFrom: { name: 'Wrocław Główny' },
            placeTo: { name: 'Zakopane' },
            price: 23.5
          }
        ]
      }
    ];

    dbo.collection('travels').insertMany(travels, function(err, res) {
      if (err) throw err;
      console.log('Number of documents inserted: ' + res.insertedCount);
      db.close();
    });
  }
);
