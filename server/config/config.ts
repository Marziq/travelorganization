const dotenv = require('dotenv');

function load() {
  console.log(`NODE_ENV ${process.env.NODE_ENV}`);
  if (
    process.env.NODE_ENV === 'production' ||
    process.env.NODE_ENV === 'prod'
  ) {
    dotenv.config({ path: __dirname + '/.env.production' });
  } else if (process.env.NODE_ENV === 'test') {
    console.log(__dirname + '.env.test');
    dotenv.config({ path: __dirname + '/.env.test' });
  } else {
    dotenv.config({ path: __dirname + '/.env.development' });
  }
}

module.exports = load();

// NODE_ENV=prod node app.js // PROD
// NODE_ENV=test node app.js // TEST
// node app.js // DEV
