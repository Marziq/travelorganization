import { Component, Renderer2 } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(private renderer: Renderer2, private _router: Router) {
    this._router.events.subscribe(event => {
      if (event instanceof NavigationStart) {
        this.renderer.removeAttribute(document.body, 'class');
        let currentUrlSlug = event.url.slice(1);
        if (currentUrlSlug.indexOf('?') !== -1) {
          currentUrlSlug = currentUrlSlug.substring(
            0,
            currentUrlSlug.indexOf('?')
          );
        }
        const currentUrlArray = currentUrlSlug.split('/');
        if (currentUrlSlug) {
          if (currentUrlArray.length > 1) {
            this.renderer.addClass(document.body, currentUrlArray[1]);
          } else {
            this.renderer.addClass(document.body, currentUrlSlug);
          }
        }
      }
    });
  }
}
