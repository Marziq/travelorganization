import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../../services/auth.service';
import { User } from '../../services/models/user.model';
import { map } from 'rxjs/operators';
import { Alert } from '../../services/models/alert.model';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  userFormModel: FormGroup;
  clickedSubmitForm = false;
  wrongDataToLogin = false;
  alerts: Alert[] = [];
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private alertService: AlertService,
    private _router: Router,
    private route: ActivatedRoute
  ) {
    this.userFormModel = fb.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {
    /*     if (this.authService.isLoggedIn) {
      this._router.navigate(['home']);
    } */
  }

  onSubmit() {
    if (this.userFormModel.valid) {
      const user = new User(
        this.userFormModel.value.email,
        '',
        '',
        [],
        this.userFormModel.value.password
      );
      this.authService.login(user).subscribe(
        result => {
          this.alertService.success(result.message);
          this.authService.setLoggedIn(true);
          this._router.navigate(['home']);
        },
        err => {
          this.alertService.error(err.error.message);
          this.wrongDataToLogin = true;
          this.clickedSubmitForm = true;
        }
      );
    } else {
      this.clickedSubmitForm = true;
    }
  }
}
