import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TravelComponent } from './travel/travel.component';
import { TravelsComponent } from './travels.component';

import { TravelService } from '../../services/travel.service';

import { TravelRoutingModule } from './travel-routing.module';
import { SortModule } from './../pipes/sort.module';
import { FilterModule } from '../pipes/filter.module';
import { CalendarModule } from 'primeng/calendar';
import { CheckboxModule } from 'primeng/checkbox';
import { InputTextModule } from 'primeng/inputtext';
import { AgmCoreModule } from '@agm/core';
import { NgbModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { TravelCreateComponent } from './travel-create/travel-create.component';
import { ThingToPackFormComponent } from './travel-form/thing-to-pack-form/thing-to-pack-form.component';
import { AccommodationFormComponent } from './travel-form/accommodation-form/accommodation-form.component';
import { TransportFormComponent } from './travel-form/transport-form/transport-form.component';
import { AttractionFormComponent } from './travel-form/attraction-form/attraction-form.component';
import { FoodFormComponent } from './travel-form/food-form/food-form.component';
import { EditFoodComponent } from './travel-edit/edit-food/edit-food.component';
import { EditAccommodationComponent } from './travel-edit/edit-accommodation/edit-accommodation.component';
import { EditAttractionsComponent } from './travel-edit/edit-attractions/edit-attractions.component';
import { EditTransportComponent } from './travel-edit/edit-transport/edit-transport.component';
import { EditThingsToPackComponent } from './travel-edit/edit-things-to-pack/edit-things-to-pack.component';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { EditTravelInfoComponent } from './travel-edit/edit-travel-info/edit-travel-info.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    TravelRoutingModule,
    SortModule,
    FilterModule,
    CalendarModule,
    CheckboxModule,
    InputTextModule,
    NgbModule,
    NgbAlertModule,
    AngularSvgIconModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCPK6vpfTjRJ2rxGj4lJCAd5w0RlY5Fs_s',
      libraries: ['places']
    })
  ],
  declarations: [
    TravelComponent,
    TravelCreateComponent,
    TravelsComponent,
    ThingToPackFormComponent,
    AccommodationFormComponent,
    TransportFormComponent,
    AttractionFormComponent,
    FoodFormComponent,
    EditFoodComponent,
    EditAccommodationComponent,
    EditAttractionsComponent,
    EditTransportComponent,
    EditThingsToPackComponent,
    EditTravelInfoComponent
  ],
  providers: [TravelService],
  entryComponents: [
    EditFoodComponent,
    EditAccommodationComponent,
    EditAttractionsComponent,
    EditTransportComponent,
    EditThingsToPackComponent,
    EditTravelInfoComponent
  ]
})
export class TravelsModule {}
