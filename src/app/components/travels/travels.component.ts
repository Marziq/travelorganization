import { AuthService } from './../../services/auth.service';
import { TravelService } from '../../services/travel.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Travel } from '../../services/models/travel.model';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ModalAlertComponent } from '../modal-alert/modal-alert.component';
import { ActivatedRoute } from '@angular/router';
import { Alert } from '../../services/models/alert.model';
import { AlertService } from '../../services/alert.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-travels',
  templateUrl: './travels.component.html',
  styleUrls: ['./travels.component.scss']
})
export class TravelsComponent implements OnInit, OnDestroy {
  travels: Travel[];
  travelSearch: String;
  private subscription: Subscription;

  constructor(
    private travelService: TravelService,
    private authService: AuthService,
    private alertService: AlertService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.subscription = this.travelService.getTravels().subscribe(result => {
      this.travels = result;
    });
  }

  removeTravel(travel: Travel) {
    const modalRef = this.modalService.open(ModalAlertComponent);
    modalRef.componentInstance.name = travel.title;

    modalRef.result.then(result => {
      if (result === 'remove') {
        this.travelService.removeTravel(travel.mongoID).subscribe(
          data => {
            console.log(data);
            this.travelService.removeTravelByID(travel.mongoID);
            this.alertService.success(data.message);
          },
          err => {
            if (err.status === 401) {
              this.alertService.warn(err.error.message, true);
              this.authService.goToLoginPage();
            } else {
              this.alertService.error(err.error.message);
            }
          }
        );
      }
    });
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
