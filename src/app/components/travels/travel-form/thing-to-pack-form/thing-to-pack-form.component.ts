import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-thing-to-pack-form',
  templateUrl: './thing-to-pack-form.component.html',
  styleUrls: ['./thing-to-pack-form.component.css']
})
export class ThingToPackFormComponent implements OnInit {
  @Input()
  formModel: FormGroup;
  @Input()
  clickedSubmitForm: boolean;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {}

  addThing() {
    (<FormArray>this.formModel.controls['thingsToPack']).push(
      this.fb.group({
        name: ['', Validators.required],
        taken: [false]
      })
    );
  }

  removeElement(index) {
    console.log(index);
    (<FormArray>this.formModel.controls['thingsToPack']).removeAt(index);
  }
}
