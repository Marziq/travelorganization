import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ThingToPackFormComponent } from './thing-to-pack-form.component';

describe('ThingToPackFormComponent', () => {
  let component: ThingToPackFormComponent;
  let fixture: ComponentFixture<ThingToPackFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ThingToPackFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ThingToPackFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
