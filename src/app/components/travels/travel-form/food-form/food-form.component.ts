import { Component, OnInit, Input } from '@angular/core';
import { FormArray, FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-food-form',
  templateUrl: './food-form.component.html',
  styleUrls: ['./food-form.component.css']
})
export class FoodFormComponent implements OnInit {
  @Input()
  formModel: FormGroup;
  @Input()
  clickedSubmitForm: boolean;
  @Input()
  calendarModel: any;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {}

  addFood() {
    (<FormArray>this.formModel.controls['food']).push(
      this.fb.group({
        name: ['', Validators.required],
        price: [0],
        date: ['']
      })
    );
  }

  removeElement(index) {
    (<FormArray>this.formModel.controls['food']).removeAt(index);
  }
}
