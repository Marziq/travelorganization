import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-transport-form',
  templateUrl: './transport-form.component.html',
  styleUrls: ['./transport-form.component.css']
})
export class TransportFormComponent implements OnInit {
  @Input()
  formModel: FormGroup;
  @Input()
  clickedSubmitForm: boolean;
  @Input()
  calendarModel: any;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {}

  addTransport() {
    (<FormArray>this.formModel.controls['transport']).push(
      this.fb.group({
        type: ['', Validators.required],
        price: [0],
        date: [''],
        placeFrom: ['', Validators.required],
        placeTo: ['', Validators.required]
      })
    );
  }

  removeElement(index) {
    (<FormArray>this.formModel.controls['transport']).removeAt(index);
  }
}
