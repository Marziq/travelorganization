import { Component, OnInit, Input } from '@angular/core';
import { FormArray, FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-accommodation-form',
  templateUrl: './accommodation-form.component.html',
  styleUrls: ['./accommodation-form.component.css']
})
export class AccommodationFormComponent implements OnInit {
  @Input()
  formModel: FormGroup;
  @Input()
  clickedSubmitForm: boolean;
  @Input()
  calendarModel: any;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {}

  addAccommodation() {
    (<FormArray>this.formModel.controls['accommodations']).push(
      this.fb.group({
        price: [0],
        date: ['', Validators.required],
        placeName: ['', Validators.required],
        placeAddress: ['']
      })
    );
  }

  removeElement(index) {
    (<FormArray>this.formModel.controls['accommodations']).removeAt(index);
  }
}
