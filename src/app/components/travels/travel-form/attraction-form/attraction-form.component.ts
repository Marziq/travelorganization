import { Component, OnInit, Input } from '@angular/core';
import { FormArray, FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-attraction-form',
  templateUrl: './attraction-form.component.html',
  styleUrls: ['./attraction-form.component.css']
})
export class AttractionFormComponent implements OnInit {
  @Input()
  formModel: FormGroup;
  @Input()
  clickedSubmitForm: boolean;
  @Input()
  calendarModel: any;
  @Input()
  isModalWindow = false;

  constructor(private fb: FormBuilder) {}

  ngOnInit() {}

  addAttraction() {
    (<FormArray>this.formModel.controls['attractions']).push(
      this.fb.group({
        name: ['', Validators.required],
        price: [0],
        date: [''],
        address: ['']
      })
    );
  }

  removeElement(index) {
    (<FormArray>this.formModel.controls['attractions']).removeAt(index);
  }
}
