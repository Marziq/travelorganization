import { ImageService } from './../../../services/image.service';
import { EditTransportComponent } from './../travel-edit/edit-transport/edit-transport.component';
import { EditAttractionsComponent } from './../travel-edit/edit-attractions/edit-attractions.component';
import { TravelService } from '../../../services/travel.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Travel } from '../../../services/models/travel.model';
import { MappingService } from '../../../services/mapping.service';
import { Alert } from '../../../services/models/alert.model';
import { AuthService } from '../../../services/auth.service';
import { AlertService } from '../../../services/alert.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EditFoodComponent } from '../travel-edit/edit-food/edit-food.component';
import { EditThingsToPackComponent } from '../travel-edit/edit-things-to-pack/edit-things-to-pack.component';
import { EditAccommodationComponent } from '../travel-edit/edit-accommodation/edit-accommodation.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { EditTravelInfoComponent } from '../travel-edit/edit-travel-info/edit-travel-info.component';

@Component({
  selector: 'app-travel',
  templateUrl: './travel.component.html',
  styleUrls: ['./travel.component.css']
})
export class TravelComponent implements OnInit {
  travel: Travel;
  travelID: string;

  constructor(
    private travelService: TravelService,
    private route: ActivatedRoute,
    private mappingService: MappingService,
    private authService: AuthService,
    private alertService: AlertService,
    private modalService: NgbModal
  ) {}

  ngOnInit() {
    this.travelID = this.route.snapshot.params['id'];
    this.travelService.getTravel(this.travelID).subscribe(
      result => {
        if (result.travel) {
          this.travel = this.mappingService.mapTravelFromBackend(
            result.travel[0]
          );
        } else {
          this.travel = result;
        }
      },
      err => {
        if (err.status === 401) {
          this.authService.goToLoginPage();
          this.alertService.warn(err.error.message, true);
        }
        this.alertService.error(err.error.message);
      }
    );
  }

  editFood(travel: Travel) {
    const modalRef = this.modalService.open(EditFoodComponent, { size: 'lg' });
    modalRef.componentInstance.travelID = travel.mongoID;
    modalRef.componentInstance.food = travel.food;

    modalRef.result.then(result => {
      if (result.message === 'save') {
        travel.food = result.food;
      }
    });
  }
  editThingsToPack(travel: Travel) {
    const modalRef = this.modalService.open(EditThingsToPackComponent, {
      size: 'lg'
    });
    modalRef.componentInstance.travelID = travel.mongoID;
    modalRef.componentInstance.thingsToPack = travel.thingsToPack;

    modalRef.result.then(result => {
      if (result.message === 'save') {
        travel.thingsToPack = result.thingsToPack;
      }
    });
  }

  totalSumOfTravel() {
    return this.travelService.getTotalPrice(
      this.travel,
      null,
      null,
      null,
      null
    );
  }

  totalSum(items, isAccommodation = false) {
    return this.travelService.totalSum(items, isAccommodation);
  }

  editAttractions(travel: Travel) {
    const modalRef = this.modalService.open(EditAttractionsComponent, {
      size: 'lg'
    });
    modalRef.componentInstance.travelID = travel.mongoID;
    modalRef.componentInstance.attractions = travel.attractions;

    modalRef.result.then(result => {
      if (result.message === 'save') {
        travel.attractions = result.attractions;
      }
    });
  }

  editTransport(travel: Travel) {
    const modalRef = this.modalService.open(EditTransportComponent, {
      size: 'lg'
    });
    modalRef.componentInstance.travelID = travel.mongoID;
    modalRef.componentInstance.transport = travel.transport;

    modalRef.result.then(result => {
      if (result.message === 'save') {
        travel.transport = result.transport;
      }
    });
  }

  editAccommodations(travel: Travel) {
    const modalRef = this.modalService.open(EditAccommodationComponent, {
      size: 'lg'
    });
    modalRef.componentInstance.travelID = travel.mongoID;
    modalRef.componentInstance.accommodations = travel.accommodations;

    modalRef.result.then(result => {
      if (result.message === 'save') {
        travel.accommodations = result.accommodations;
      }
    });
  }

  editDescription(travel: Travel) {
    const modalRef = this.modalService.open(EditTravelInfoComponent, {
      size: 'lg'
    });
    modalRef.componentInstance.travelID = travel.mongoID;
    modalRef.componentInstance.travel = travel;

    modalRef.result.then(result => {
      if (result.message === 'save') {
        travel.title = result.travel.title;
        travel.place.name = result.travel.place.name;
        travel.place.address = result.travel.place.address;
        travel.price = result.travel.price;
        travel.description = result.travel.description;
        travel.dateFrom = result.travel.dateFrom;
        travel.dateTo = result.travel.dateTo;
      }
    });
  }

  isUndefined(obj) {
    return obj === null || typeof obj === 'undefined';
  }
}
