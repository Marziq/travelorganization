import { Accommodation } from './../../../services/models/accommodation.model';
import { Place } from './../../../services/models/place.model';
import { TravelService } from './../../../services/travel.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Travel } from '../../../services/models/travel.model';
import { Transport } from '../../../services/models/transport.model';
import { ThingToPack } from '../../../services/models/thing-to-pack.model';
import { Food } from '../../../services/models/food.model';
import { Attraction } from '../../../services/models/attraction.model';
import { AlertService } from '../../../services/alert.service';
import { AuthService } from '../../../services/auth.service';
import { SharedService } from '../../../services/shared.service';

@Component({
  selector: 'app-travel-create',
  templateUrl: './travel-create.component.html',
  styleUrls: ['./travel-create.component.css']
})
export class TravelCreateComponent implements OnInit {
  travelPlace: string;
  pageTitle: string;
  buttonTitle: string;
  sumPrice = 0.0;
  clickedSubmitForm = false;
  pl: any;
  public formModel: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private travelService: TravelService,
    private alertService: AlertService,
    private authService: AuthService,
    private sharedService: SharedService,
    private _router: Router,
    private fb: FormBuilder
  ) {
    this.travelPlace = route.snapshot.params['place'];
    this.formModel = fb.group({
      travelTitle: ['', Validators.required],
      travelPlaceName: [
        !this.travelPlace ? '' : this.travelPlace,
        Validators.required
      ],
      travelPlaceAddress: [''],
      travelDate: ['', Validators.required],
      travelPrice: [0],
      travelDescription: [''],
      thingsToPack: fb.array([
        fb.group({
          name: ['', Validators.required],
          taken: [false]
        })
      ]),
      accommodations: fb.array([
        fb.group({
          price: [0],
          date: ['', Validators.required],
          placeName: ['', Validators.required],
          placeAddress: ['']
        })
      ]),
      transport: fb.array([
        fb.group({
          type: ['', Validators.required],
          price: [0],
          date: [''],
          placeFrom: ['', Validators.required],
          placeTo: ['', Validators.required]
        })
      ]),
      attractions: fb.array([
        fb.group({
          name: ['', Validators.required],
          price: [0],
          date: [''],
          address: ['']
        })
      ]),
      food: fb.array([
        fb.group({
          name: ['', Validators.required],
          price: [0],
          date: ['']
        })
      ])
    });
  }

  ngOnInit() {
    this.route.data.subscribe(v => {
      this.pageTitle = v.title;
      this.buttonTitle = v.button;
    });

    this.pl = this.sharedService.getCalendarPL();
  }

  removeElement(nameOfArray, index) {
    (<FormArray>this.formModel.controls[nameOfArray]).removeAt(index);
  }

  getThingToPack(thingsData): ThingToPack[] {
    const thingsToPack: ThingToPack[] = [];
    thingsData.forEach(thing => {
      thingsToPack.push(new ThingToPack(thing.name, thing.taken));
    });
    return thingsToPack;
  }

  getAccommodations(accommodationsData): Accommodation[] {
    const accommodations: Accommodation[] = [];
    accommodationsData.forEach(accomodation => {
      accommodations.push(
        new Accommodation(
          this.getDateFrom(accomodation.date),
          this.getDateTo(accomodation.date),
          new Place(
            accomodation.placeName,
            !accomodation.placeAddress ? '' : accomodation.placeAddress
          ),
          !accomodation.price ? 0 : accomodation.price
        )
      );
    });
    return accommodations;
  }

  getTransport(transportData): Transport[] {
    const transport: Transport[] = [];

    transportData.forEach(t => {
      transport.push(
        new Transport(
          t.type,
          this.getDateFrom(t.date),
          this.getDateToForTransport(t.date),
          new Place(t.placeFrom),
          new Place(t.placeTo),
          !t.price ? 0 : t.price
        )
      );
    });
    return transport;
  }

  getAttractions(attractionsData): Attraction[] {
    const attractions: Attraction[] = [];
    attractionsData.forEach(attraction => {
      attractions.push(
        new Attraction(
          attraction.name,
          this.getDate(attraction.date),
          !attraction.price ? 0 : attraction.price,
          new Place(!attraction.address ? '' : attraction.address)
        )
      );
    });
    return attractions;
  }

  getFood(foodData): Food[] {
    const food: Food[] = [];
    foodData.forEach(item => {
      food.push(
        new Food(
          item.name,
          this.getDate(item.date),
          !item.price ? 0 : item.price
        )
      );
    });
    return food;
  }

  getDate(date: any): Date {
    return date !== undefined && date !== null && date !== ''
      ? new Date(date)
      : null;
  }

  getDateFrom(dates: any[]): Date {
    return this.getDate(dates[0]);
  }

  getDateToForTransport(dates: any[]): Date {
    return dates[1] !== undefined && dates[1] !== null && dates[1] !== ''
      ? new Date(dates[1])
      : null;
  }

  getDateTo(dates: any[]): Date {
    return dates[1] !== undefined && dates[1] !== null && dates[1] !== ''
      ? new Date(dates[1])
      : this.getDate(dates[0]);
  }

  onSubmit() {
    if (this.formModel.valid) {
      const travelData = this.formModel.value;
      const travel = Travel.create(
        '',
        travelData.travelTitle,
        this.getDateFrom(travelData.travelDate),
        this.getDateTo(travelData.travelDate),
        new Place(
          travelData.travelPlaceName,
          !travelData.travelPlaceAddress ? '' : travelData.travelPlaceAddress
        ),
        !travelData.travelDescription ? '' : travelData.travelDescription,
        !travelData.travelPrice ? 0 : travelData.travelPrice,
        this.getThingToPack(travelData.thingsToPack),
        this.getTransport(travelData.transport),
        this.getAccommodations(travelData.accommodations),
        this.getFood(travelData.food),
        this.getAttractions(travelData.attractions)
      );
      this.travelService.addTravel(travel).subscribe(
        result => {
          travel.mongoID = result.travelID;
          this.travelService.pushTravelToList(travel);
          this.alertService.success(result.message, true);
          this._router.navigate(['travels']);
        },
        err => {
          if (err.status === 401) {
            this.alertService.warn(err.error.message, true);
            this.authService.goToLoginPage();
          } else {
            this.alertService.error(err.error.message);
          }
        }
      );
    } else {
      this.clickedSubmitForm = true;
    }
  }

  summaryOfExpenses() {
    let sum = 0.0;
    sum += +this.formModel.value.travelPrice;
    this.formModel.value.transport.forEach(transport => {
      sum += transport.price ? +transport.price : +0;
    });

    this.formModel.value.accommodations.forEach(accommodation => {
      const milisecPerDay = 24 * 60 * 60 * 1000;
      const dateTo = this.getDateTo(accommodation.date);
      const dateFrom = this.getDateFrom(accommodation.date);
      if (dateFrom == null || dateTo.getTime() === dateFrom.getTime()) {
        sum += accommodation.price ? +accommodation.price : +0;
      } else {
        const days = (dateTo.getTime() - dateFrom.getTime()) / milisecPerDay;
        sum += accommodation.price ? +accommodation.price * days : +0;
      }
    });

    this.formModel.value.food.forEach(food => {
      sum += food.price ? +food.price : +0;
    });

    this.formModel.value.attractions.forEach(attraction => {
      sum += attraction.price ? +attraction.price : +0;
    });
    return sum;
  }
}
