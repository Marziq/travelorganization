import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditAttractionsComponent } from './edit-attractions.component';

describe('EditAttractionsComponent', () => {
  let component: EditAttractionsComponent;
  let fixture: ComponentFixture<EditAttractionsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditAttractionsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditAttractionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
