import { SharedService } from './../../../../services/shared.service';
import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { TravelService } from '../../../../services/travel.service';
import { AlertService } from '../../../../services/alert.service';
import { AuthService } from '../../../../services/auth.service';
import { Attraction } from '../../../../services/models/attraction.model';
import { Place } from '../../../../services/models/place.model';

@Component({
  selector: 'app-edit-attractions',
  templateUrl: './edit-attractions.component.html',
  styleUrls: ['./edit-attractions.component.css']
})
export class EditAttractionsComponent implements OnInit {
  @Input()
  travelID: string;
  @Input()
  attractions: Attraction[];
  formModel: FormGroup;
  clickedSubmitForm = false;
  pl: any;

  constructor(
    public activeModal: NgbActiveModal,
    private travelService: TravelService,
    private alertService: AlertService,
    private authService: AuthService,
    private sharedService: SharedService,
    private fb: FormBuilder
  ) {
    this.formModel = fb.group({
      attractions: fb.array([
        fb.group({
          name: ['', Validators.required],
          price: [0],
          date: [''],
          address: ['']
        })
      ])
    });

    this.pl = sharedService.getCalendarPL();
  }

  ngOnInit(): void {
    if (
      typeof this.attractions !== 'undefined' &&
      this.attractions.length !== 0
    ) {
      const attractionsControl = <FormArray>this.formModel.controls.attractions;
      attractionsControl.removeAt(0);
      this.attractions.forEach(a => {
        attractionsControl.push(
          this.fb.group({
            name: a.name,
            price: a.price,
            date: a.date,
            address: a.place.name
          })
        );
      });
    }
  }

  getDate(date: any): Date {
    return date !== undefined && date !== null && date !== ''
      ? new Date(date)
      : null;
  }

  getAttractions(attractionsData): Attraction[] {
    const attractions: Attraction[] = [];
    attractionsData.forEach(attraction => {
      attractions.push(
        new Attraction(
          attraction.name,
          this.getDate(attraction.date),
          !attraction.price ? 0 : attraction.price,
          new Place(!attraction.address ? '' : attraction.address)
        )
      );
    });
    return attractions;
  }

  onSubmit() {
    if (this.formModel.valid) {
      const travelData = this.formModel.value;
      const attractions = this.getAttractions(travelData.attractions);
      console.log(attractions);
      this.travelService
        .updateTravelAttractions(this.travelID, attractions)
        .subscribe(
          result => {
            console.log(result);
            this.alertService.success(result.message);
            this.activeModal.close({
              message: 'save',
              attractions: attractions
            });
          },
          err => {
            if (err.status === 401) {
              this.alertService.warn(err.error.message, true);
              this.authService.goToLoginPage();
            } else {
              this.alertService.error(err.error.message);
            }
          }
        );
    } else {
      this.clickedSubmitForm = true;
    }
  }
}
