import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTravelInfoComponent } from './edit-travel-info.component';

describe('EditTravelInfoComponent', () => {
  let component: EditTravelInfoComponent;
  let fixture: ComponentFixture<EditTravelInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTravelInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTravelInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
