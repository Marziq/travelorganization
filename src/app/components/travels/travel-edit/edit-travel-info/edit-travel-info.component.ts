import { Component, OnInit, Input } from '@angular/core';
import { Travel } from 'src/app/services/models/travel.model';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TravelService } from 'src/app/services/travel.service';
import { AlertService } from 'src/app/services/alert.service';
import { AuthService } from 'src/app/services/auth.service';
import { SharedService } from 'src/app/services/shared.service';
import { Place } from 'src/app/services/models/place.model';

@Component({
  selector: 'app-edit-travel-info',
  templateUrl: './edit-travel-info.component.html',
  styleUrls: ['./edit-travel-info.component.css']
})
export class EditTravelInfoComponent implements OnInit {
  @Input()
  travelID: string;
  @Input()
  travel: Travel;
  formModel: FormGroup;
  clickedSubmitForm = false;
  pl: any;

  constructor(
    public activeModal: NgbActiveModal,
    private travelService: TravelService,
    private alertService: AlertService,
    private authService: AuthService,
    private fb: FormBuilder,
    private sharedService: SharedService
  ) {
    this.pl = this.sharedService.getCalendarPL();
  }

  ngOnInit(): void {
    this.formModel = this.fb.group({
      title: [this.travel.title, Validators.required],
      placeName: [this.travel.place.name, Validators.required],
      placeAddress: [this.travel.place.address],
      date: [[this.travel.dateFrom, this.travel.dateTo], Validators.required],
      price: [this.travel.price],
      description: [this.travel.description]
    });
  }

  getDate(date: any): Date {
    return date !== undefined && date !== null && date !== ''
      ? new Date(date)
      : null;
  }

  getTravel(travelData): Travel {
    let travel = new Travel();
    travel.title = travelData.title;
    travel.place = new Place(travelData.placeName, travelData.placeAddress);
    travel.price = travelData.price;
    travel.description = travelData.description;
    travel.dateFrom = this.getDate(travelData.date[0]);
    travel.dateTo = this.getDate(travelData.date[1]);
    return travel;
  }

  onSubmit() {
    if (this.formModel.valid) {
      const travelData = this.formModel.value;
      const travel = this.getTravel(travelData);
      console.log(travel);
      this.travelService.updateTravelInfo(this.travelID, travel).subscribe(
        result => {
          console.log(result);
          this.alertService.success(result.message);
          this.activeModal.close({ message: 'save', travel: travel });
        },
        err => {
          if (err.status === 401) {
            this.alertService.warn(err.error.message, true);
            this.authService.goToLoginPage();
          } else {
            this.alertService.error(err.error.message);
          }
        }
      );
    } else {
      this.clickedSubmitForm = true;
    }
  }
}
