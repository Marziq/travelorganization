import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { TravelService } from '../../../../services/travel.service';
import { AlertService } from '../../../../services/alert.service';
import { AuthService } from '../../../../services/auth.service';
import { Food } from '../../../../services/models/food.model';
import { SharedService } from '../../../../services/shared.service';

@Component({
  selector: 'app-edit-food',
  templateUrl: './edit-food.component.html',
  styleUrls: ['./edit-food.component.css']
})
export class EditFoodComponent implements OnInit {
  @Input()
  travelID: string;
  @Input()
  food: Food[];
  formModel: FormGroup;
  clickedSubmitForm = false;
  pl: any;

  constructor(
    public activeModal: NgbActiveModal,
    private travelService: TravelService,
    private alertService: AlertService,
    private authService: AuthService,
    private sharedService: SharedService,
    private fb: FormBuilder
  ) {
    this.formModel = fb.group({
      food: fb.array([
        fb.group({
          name: ['', Validators.required],
          price: [0],
          date: ['']
        })
      ])
    });

    this.pl = sharedService.getCalendarPL();
  }

  ngOnInit(): void {
    if (typeof this.food !== 'undefined' && this.food.length !== 0) {
      const foodControl = <FormArray>this.formModel.controls.food;
      foodControl.removeAt(0);
      this.food.forEach(a => {
        foodControl.push(
          this.fb.group({
            name: a.name,
            price: a.price,
            date: a.date
          })
        );
      });
    }
  }

  getDate(date: any): Date {
    return date !== undefined && date !== null && date !== ''
      ? new Date(date)
      : null;
  }

  getFood(foodData): Food[] {
    const food: Food[] = [];
    foodData.forEach(item => {
      food.push(
        new Food(
          item.name,
          this.getDate(item.date),
          !item.price ? 0 : item.price
        )
      );
    });
    return food;
  }

  onSubmit() {
    if (this.formModel.valid) {
      const travelData = this.formModel.value;
      const food = this.getFood(travelData.food);
      console.log(food);
      this.travelService.updateTravelFood(this.travelID, food).subscribe(
        result => {
          console.log(result);
          this.alertService.success(result.message);
          this.activeModal.close({ message: 'save', food: food });
        },
        err => {
          if (err.status === 401) {
            this.alertService.warn(err.error.message, true);
            this.authService.goToLoginPage();
          } else {
            this.alertService.error(err.error.message);
          }
        }
      );
    } else {
      this.clickedSubmitForm = true;
    }
  }
}
