import { SharedService } from './../../../../services/shared.service';
import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { TravelService } from '../../../../services/travel.service';
import { AlertService } from '../../../../services/alert.service';
import { AuthService } from '../../../../services/auth.service';
import { Food } from '../../../../services/models/food.model';
import { ThingToPack } from '../../../../services/models/thing-to-pack.model';

@Component({
  selector: 'app-edit-things-to-pack',
  templateUrl: './edit-things-to-pack.component.html',
  styleUrls: ['./edit-things-to-pack.component.css']
})
export class EditThingsToPackComponent implements OnInit {
  @Input()
  travelID: string;
  @Input()
  thingsToPack: ThingToPack[];
  formModel: FormGroup;
  clickedSubmitForm = false;
  pl: any;

  constructor(
    public activeModal: NgbActiveModal,
    private travelService: TravelService,
    private alertService: AlertService,
    private authService: AuthService,
    private fb: FormBuilder
  ) {
    this.formModel = fb.group({
      thingsToPack: fb.array([
        fb.group({
          name: ['', Validators.required],
          taken: [false]
        })
      ])
    });
  }

  ngOnInit(): void {
    if (
      typeof this.thingsToPack !== 'undefined' &&
      this.thingsToPack.length !== 0
    ) {
      const thingsToPackControl = <FormArray>(
        this.formModel.controls.thingsToPack
      );
      thingsToPackControl.removeAt(0);
      this.thingsToPack.forEach(a => {
        thingsToPackControl.push(
          this.fb.group({
            name: a.name,
            taken: a.taken
          })
        );
      });
    }
  }

  getDate(date: any): Date {
    return date !== undefined && date !== null && date !== ''
      ? new Date(date)
      : null;
  }

  getThingToPack(thingsData): ThingToPack[] {
    const thingsToPack: ThingToPack[] = [];
    thingsData.forEach(thing => {
      thingsToPack.push(new ThingToPack(thing.name, thing.taken));
    });
    return thingsToPack;
  }

  onSubmit() {
    if (this.formModel.valid) {
      const travelData = this.formModel.value;
      const thingsToPack = this.getThingToPack(travelData.thingsToPack);
      console.log(thingsToPack);
      this.travelService
        .updateTravelThingsToPack(this.travelID, thingsToPack)
        .subscribe(
          result => {
            console.log(result);
            this.alertService.success(result.message);
            this.activeModal.close({
              message: 'save',
              thingsToPack: thingsToPack
            });
          },
          err => {
            if (err.status === 401) {
              this.alertService.warn(err.error.message, true);
              this.authService.goToLoginPage();
            } else {
              this.alertService.error(err.error.message);
            }
          }
        );
    } else {
      this.clickedSubmitForm = true;
    }
  }
}
