import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditThingsToPackComponent } from './edit-things-to-pack.component';

describe('EditThingsToPackComponent', () => {
  let component: EditThingsToPackComponent;
  let fixture: ComponentFixture<EditThingsToPackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditThingsToPackComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditThingsToPackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
