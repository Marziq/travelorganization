import { Component, OnInit, Input } from '@angular/core';
import { Transport } from '../../../../services/models/transport.model';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TravelService } from '../../../../services/travel.service';
import { AlertService } from '../../../../services/alert.service';
import { AuthService } from '../../../../services/auth.service';
import { SharedService } from '../../../../services/shared.service';
import { Place } from '../../../../services/models/place.model';

@Component({
  selector: 'app-edit-transport',
  templateUrl: './edit-transport.component.html',
  styleUrls: ['./edit-transport.component.css']
})
export class EditTransportComponent implements OnInit {
  @Input()
  travelID: string;
  @Input()
  transport: Transport[];
  formModel: FormGroup;
  clickedSubmitForm = false;
  pl: any;

  constructor(
    public activeModal: NgbActiveModal,
    private travelService: TravelService,
    private alertService: AlertService,
    private authService: AuthService,
    private sharedService: SharedService,
    private fb: FormBuilder
  ) {
    this.formModel = fb.group({
      transport: fb.array([
        fb.group({
          type: ['', Validators.required],
          price: [0],
          date: [''],
          placeFrom: ['', Validators.required],
          placeTo: ['', Validators.required]
        })
      ])
    });
    this.pl = this.sharedService.getCalendarPL();
  }

  ngOnInit(): void {
    if (typeof this.transport !== 'undefined' && this.transport.length !== 0) {
      const transportControl = <FormArray>this.formModel.controls.transport;
      transportControl.removeAt(0);
      this.transport.forEach(t => {
        transportControl.push(
          this.fb.group({
            type: t.type,
            price: t.price,
            date: [new Date(t.dateFrom), new Date(t.dateTo)],
            placeFrom: t.placeFrom.name,
            placeTo: t.placeTo.name
          })
        );
      });
    }
  }

  getDate(date: any): Date {
    return date !== undefined && date !== null && date !== ''
      ? new Date(date)
      : null;
  }

  getDateFrom(dates: any[]): Date {
    return this.getDate(dates[0]);
  }

  getDateToForTransport(dates: any[]): Date {
    return dates[1] !== undefined && dates[1] !== null && dates[1] !== ''
      ? new Date(dates[1])
      : null;
  }

  getTransport(transportData): Transport[] {
    const transport: Transport[] = [];

    transportData.forEach(t => {
      transport.push(
        new Transport(
          t.type,
          this.getDateFrom(t.date),
          this.getDateToForTransport(t.date),
          new Place(t.placeFrom),
          new Place(t.placeTo),
          !t.price ? 0 : t.price
        )
      );
    });
    return transport;
  }

  onSubmit() {
    if (this.formModel.valid) {
      const travelData = this.formModel.value;
      const transport = this.getTransport(travelData.transport);
      console.log(transport);
      this.travelService
        .updateTravelTransport(this.travelID, transport)
        .subscribe(
          result => {
            console.log(result);
            this.alertService.success(result.message);
            this.activeModal.close({
              message: 'save',
              transport: transport
            });
          },
          err => {
            if (err.status === 401) {
              this.alertService.warn(err.error.message, true);
              this.authService.goToLoginPage();
            } else {
              this.alertService.error(err.error.message);
            }
          }
        );
    } else {
      this.clickedSubmitForm = true;
    }
  }
}
