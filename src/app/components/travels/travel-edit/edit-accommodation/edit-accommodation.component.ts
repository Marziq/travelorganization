import { SharedService } from './../../../../services/shared.service';
import { Component, OnInit, Input } from '@angular/core';
import { Accommodation } from '../../../../services/models/accommodation.model';
import { FormGroup, FormBuilder, Validators, FormArray } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { TravelService } from '../../../../services/travel.service';
import { AlertService } from '../../../../services/alert.service';
import { AuthService } from '../../../../services/auth.service';
import { Place } from '../../../../services/models/place.model';

@Component({
  selector: 'app-edit-accommodation',
  templateUrl: './edit-accommodation.component.html',
  styleUrls: ['./edit-accommodation.component.css']
})
export class EditAccommodationComponent implements OnInit {
  @Input()
  travelID: string;
  @Input()
  accommodations: Accommodation[];
  formModel: FormGroup;
  clickedSubmitForm = false;
  pl: any;

  constructor(
    public activeModal: NgbActiveModal,
    private travelService: TravelService,
    private alertService: AlertService,
    private authService: AuthService,
    private sharedService: SharedService,
    private fb: FormBuilder
  ) {
    this.formModel = fb.group({
      accommodations: fb.array([
        fb.group({
          price: [0],
          date: ['', Validators.required],
          placeName: ['', Validators.required],
          placeAddress: ['']
        })
      ])
    });

    this.pl = this.sharedService.getCalendarPL();
  }

  ngOnInit(): void {
    if (
      typeof this.accommodations !== 'undefined' &&
      this.accommodations.length !== 0
    ) {
      const accommodationsControl = <FormArray>(
        this.formModel.controls.accommodations
      );
      accommodationsControl.removeAt(0);
      this.accommodations.forEach(a => {
        debugger;
        accommodationsControl.push(
          this.fb.group({
            price: a.price,
            date: [new Date(a.dateFrom), new Date(a.dateTo)],
            placeName: a.place.name,
            placeAddress: a.place.address
          })
        );
      });
    }
  }

  getDate(date: any): Date {
    return date !== undefined && date !== null && date !== ''
      ? new Date(date)
      : null;
  }

  getDateFrom(dates: any[]): Date {
    return this.getDate(dates[0]);
  }

  getDateTo(dates: any[]): Date {
    return dates[1] !== undefined && dates[1] !== null && dates[1] !== ''
      ? new Date(dates[1])
      : this.getDate(dates[0]);
  }

  getAccommodations(accommodationsData): Accommodation[] {
    const accommodations: Accommodation[] = [];
    accommodationsData.forEach(accomodation => {
      accommodations.push(
        new Accommodation(
          this.getDateFrom(accomodation.date),
          this.getDateTo(accomodation.date),
          new Place(
            accomodation.placeName,
            !accomodation.placeAddress ? '' : accomodation.placeAddress
          ),
          !accomodation.price ? 0 : accomodation.price
        )
      );
    });
    return accommodations;
  }

  onSubmit() {
    if (this.formModel.valid) {
      const travelData = this.formModel.value;
      const accommodations = this.getAccommodations(travelData.accommodations);
      console.log(accommodations);
      this.travelService
        .updateTravelAccommodations(this.travelID, accommodations)
        .subscribe(
          result => {
            console.log(result);
            this.alertService.success(result.message);
            this.activeModal.close({
              message: 'save',
              accommodations: accommodations
            });
          },
          err => {
            if (err.status === 401) {
              this.alertService.warn(err.error.message, true);
              this.authService.goToLoginPage();
            } else {
              this.alertService.error(err.error.message);
            }
          }
        );
    } else {
      this.clickedSubmitForm = true;
    }
  }
}
