import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TravelComponent } from './travel/travel.component';
import { TravelsComponent } from './travels.component';
import { AuthGuard } from '../../auth.guard';
import { TravelCreateComponent } from './travel-create/travel-create.component';

const routes: Routes = [
  {
    path: 'travels',
    component: TravelsComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'travels/travel/:id',
    component: TravelComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'travels/travel-create/:place',
    component: TravelCreateComponent,
    data: { title: 'Dodaj wycieczkę', button: 'Dodaj wycieczkę' },
    canActivate: [AuthGuard]
  },
  {
    path: 'travels/travel-create',
    component: TravelCreateComponent,
    data: { title: 'Dodaj wycieczkę', button: 'Dodaj wycieczkę' },
    canActivate: [AuthGuard]
  }
  /*   {
    path: 'travels/travel-edit/:place',
    component: TravelEditComponent,
    data: { title: 'Edytuj wycieczkę', button: 'Zapisz' },
    canActivate: [AuthGuard]
  } */
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TravelRoutingModule {}
