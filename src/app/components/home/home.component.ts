import { TravelService } from './../../services/travel.service';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  travelPlace: String;
  clickedSendPlace: Boolean = false;

  constructor(
    private _router: Router,
    private authService: AuthService,
    private travelService: TravelService,
    private route: ActivatedRoute,
    private sharedService: SharedService
  ) {}

  ngOnInit() {
    this.authService.setUser(this.route.snapshot.data['user'].user);
    this.sharedService.emitChange(this.authService.getUser());
  }

  isEmptyPlace(): Boolean {
    return (
      this.clickedSendPlace &&
      (this.travelPlace === undefined || this.travelPlace === '')
    );
  }

  navigateToCreateTravel() {
    console.log(this.travelPlace);
    if (this.travelPlace !== undefined && this.travelPlace !== '') {
      this._router.navigate(['travels/travel-create', this.travelPlace]);
    } else {
      this.clickedSendPlace = true;
    }
  }
}
