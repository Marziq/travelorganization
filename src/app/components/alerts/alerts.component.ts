import { Component, OnInit, Input } from '@angular/core';
import { Alert, AlertType } from '../../services/models/alert.model';
import { AlertService } from '../../services/alert.service';
import { debounceTime } from 'rxjs/operators';

@Component({
  selector: 'app-alerts',
  templateUrl: './alerts.component.html',
  styleUrls: ['./alerts.component.css']
})
export class AlertsComponent implements OnInit {
  alerts: Alert[] = [];

  constructor(private alertService: AlertService) {}

  ngOnInit() {
    this.alertService.getAlert().subscribe((alert: Alert) => {
      if (!alert) {
        this.alerts = [];
        return;
      }
      this.alerts.push(alert);
    });
    this.alertService
      .getAlert()
      .pipe(debounceTime(8000))
      .subscribe(() => (this.alerts = []));
  }

  getClass(alert: Alert) {
    if (!alert) {
      return;
    }
    switch (alert.type) {
      case AlertType.Error:
        return 'fa-exclamation';
      case AlertType.Info:
        return '';
      case AlertType.Warning:
        return 'fa-exclamation-triangle';
      case AlertType.Success:
        return 'fa-check';
    }
  }

  getClassAlert(alert: Alert) {
    if (!alert) {
      return;
    }
    switch (alert.type) {
      case AlertType.Error:
        return 'danger';
      case AlertType.Info:
        return 'info';
      case AlertType.Warning:
        return 'warning';
      case AlertType.Success:
        return 'success';
    }
  }

  public closeAlert(alert: Alert) {
    const index: number = this.alerts.indexOf(alert);
    this.alerts.splice(index, 1);
  }
}
