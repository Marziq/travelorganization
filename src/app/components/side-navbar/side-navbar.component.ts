import { TravelService } from './../../services/travel.service';
import { AuthService } from './../../services/auth.service';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material/sidenav';
import { Router, ActivatedRoute } from '@angular/router';
import { User } from '../../services/models/user.model';
import { SharedService } from '../../services/shared.service';

@Component({
  selector: 'app-side-navbar',
  templateUrl: './side-navbar.component.html',
  styleUrls: ['./side-navbar.component.css']
})
export class SideNavbarComponent implements OnInit {
  @ViewChild('sidenav')
  sidenav: MatSidenav;
  isSideNavOpen: boolean;
  user: User;

  constructor(
    private _router: Router,
    private authService: AuthService,
    private travelService: TravelService,
    private route: ActivatedRoute,
    private sharedService: SharedService
  ) {
    sharedService.changeEmitted$.subscribe(user => {
      this.user = user;
      console.log(user);
    });
  }

  ngOnInit() {
    if (!this.user) {
      this.user = this.authService.getUser();
    }
  }

  userChangedHandler(userData: User) {
    this.user = userData;
    console.log(userData);
  }

  close() {
    this.sidenav.close();
  }

  logout() {
    this.authService.logout().subscribe(data => {
      if (data.status) {
        this.authService.removeUser();
        this.close();
        this.travelService.clearTravels();
        this._router.navigate(['login']);
      }
    });
  }

  isLoginPage(): boolean {
    const url = this._router.url.split('?')[0];
    return url === '/login' || url === '/registration';
  }
}
