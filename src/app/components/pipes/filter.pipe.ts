import { PipeTransform, Pipe } from '@angular/core';
import { Travel } from '../../services/models/travel.model';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(array: Array<Travel>, filterText: string): Array<Travel> {
    console.log(filterText);
    if (!array) {
      return [];
    }
    if (!filterText || filterText.length === 0) {
      return array;
    }
    return array.filter(
      travel =>
        travel.title.toLowerCase().indexOf(filterText.toLowerCase()) !== -1
    );
  }
}
