import { PipeTransform, Pipe } from '@angular/core';
import { Travel } from '../../services/models/travel.model';

@Pipe({ name: 'sort' })
export class SortPipe implements PipeTransform {
  transform(array: Array<Travel>, args: string, dsc: boolean): Array<Travel> {
    if (!array) {
      return [];
    }
    array.sort((a: any, b: any) => {
      debugger;
      if ((a[args] > b[args] && dsc) || (a[args] < b[args] && !dsc)) {
        return -1;
      } else if ((a[args] < b[args] && dsc) || (a[args] > b[args] && !dsc) {
        return 1;
      } else {
        return 0;
      }
    });
    return array;
  }
}
