import { Component, OnInit } from '@angular/core';
import { TravelService } from 'src/app/services/travel.service';
import * as Chart from 'chart.js';

@Component({
  selector: 'app-expenses',
  templateUrl: './expenses.component.html',
  styleUrls: ['./expenses.component.css']
})
export class ExpensesComponent implements OnInit {
  travels: any;
  months: string[];
  years: string[] = [];
  selectedYear: number;
  selectedData: string = 'totalPrice';
  dataChart: any;
  barChartOptions: any = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  barChartType: string = 'bar';
  barChartLegend: boolean = false;
  barChartLabels: string[];
  barBackgroudColor: Array<any> = [
    {
      backgroundColor: 'rgba(230, 169, 34, 0.7)',
      borderColor: 'rgba(230, 169, 34)',
      pointBackgroundColor: 'rgba(230, 169, 34, 0.7)',
      pointBorderColor: 'rgba(230, 169, 34)',
      pointHoverBackgroundColor: 'rgba(230, 169, 34, 0.7)',
      pointHoverBorderColor: 'rgba(230, 169, 34)'
    }
  ];
  barChartData: any[] = [{ data: [] }];

  constructor(private travelService: TravelService) {
    this.months = [
      'STY',
      'LUT',
      'MAR',
      'KWI',
      'MAJ',
      'CZE',
      'LIP',
      'SIE',
      'WRZ',
      'PAŹ',
      'LIS',
      'GRU'
    ];
    this.barChartLabels = this.months;
    this.travelService.getDataForChart().subscribe(result => {
      console.log(result);
      this.travels = result;
      this.dataToChart();
      this.setNewDataForChart();
    });
  }

  ngOnInit() {}

  dataToChart() {
    var results = this.travels.reduce((results, value) => {
      var date = value.dateFrom;
      var temp = results;
      var keyYear = date.getFullYear();
      if (!temp.hasOwnProperty(keyYear)) {
        this.years.push(keyYear);
        temp[keyYear] = [];
      }
      temp = temp[keyYear];
      var key = this.months[date.getMonth() | 0];
      if (!temp.hasOwnProperty(key)) {
        delete value.dateFrom;
        delete value.dateTo;
        temp[key] = value;
      } else {
        temp = temp[key];
        temp.totalPrice += value.totalPrice;
        temp.accommodationPrice += value.accommodationPrice;
        temp.transportPrice += value.transportPrice;
        temp.foodPrice += value.foodPrice;
        temp.attractionPrice += value.attractionPrice;
      }
      return results;
    }, {});
    console.log(results);
    this.dataChart = results;
    this.years.sort();
    this.selectedYear = this.years.length - 1;
  }

  getTotalPriceByMonth() {
    let totalPrice = [];
    for (let index = 0; index < this.months.length; index++) {
      const year = this.dataChart[this.years[this.selectedYear]];
      const monthPrice = year[this.months[index]];
      totalPrice.push(monthPrice ? monthPrice[this.selectedData] : 0);
    }
    return totalPrice;
  }

  nextYear() {
    if (this.selectedYear !== this.years.length - 1) {
      this.selectedYear++;
      this.setNewDataForChart();
    }
  }

  setNewDataForChart() {
    this.barChartData = [{ data: this.getTotalPriceByMonth() }];
  }

  previousYear() {
    if (this.selectedYear !== 0) {
      this.selectedYear--;
      this.setNewDataForChart();
    }
  }

  getSelectedYear() {
    return this.years[this.selectedYear];
  }

  isLastYear() {
    return this.selectedYear === this.years.length - 1;
  }

  isFirstYear() {
    return this.selectedYear === 0;
  }

  onChangeSelect(value) {
    this.selectedData = value;
    console.log(this.selectedData);
    this.setNewDataForChart();
  }

  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }
}
