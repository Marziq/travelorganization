import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { User } from '../../services/models/user.model';
import { Alert } from '../../services/models/alert.model';
import { AlertService } from '../../services/alert.service';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  userFormModel: FormGroup;
  clickedSubmitForm = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private alertService: AlertService,
    private _router: Router
  ) {
    this.userFormModel = fb.group({
      firstName: ['', Validators.required],
      lastName: ['', []],
      email: ['', [Validators.required, Validators.email]],
      passwords: fb.group(
        {
          password: ['', Validators.required],
          confirmPassword: ['', [Validators.required]]
        },
        {
          validator: this.checkIfMatchingPasswords(
            'password',
            'confirmPassword'
          )
        }
      )
    });
  }

  ngOnInit() {}

  private checkIfMatchingPasswords(
    passwordKey: string,
    passwordConfirmationKey: string
  ) {
    return (group: FormGroup) => {
      const passwordInput = group.controls[passwordKey],
        passwordConfirmationInput = group.controls[passwordConfirmationKey];
      if (passwordInput.value !== passwordConfirmationInput.value) {
        return passwordConfirmationInput.setErrors({ notEquivalent: true });
      } else {
        return passwordConfirmationInput.setErrors(null);
      }
    };
  }

  onSubmit() {
    if (this.userFormModel.valid) {
      const user = new User(
        this.userFormModel.value.email,
        this.userFormModel.value.firstName,
        this.userFormModel.value.lastName,
        [],
        this.userFormModel.value.passwords.password
      );

      this.authService.registration(user).subscribe(
        result => {
          this.alertService.success(result.message, true);
          this._router.navigate(['login']);
        },
        err => {
          this.alertService.error(err.error.message);
        }
      );
    } else {
      this.clickedSubmitForm = true;
    }
  }
}
