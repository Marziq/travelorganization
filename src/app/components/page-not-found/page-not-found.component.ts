import { Component, OnInit, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-page-not-found',
  templateUrl: './page-not-found.component.html',
  styleUrls: ['./page-not-found.component.css']
})
export class PageNotFoundComponent implements OnInit {
  constructor(private renderer: Renderer2) {
    this.renderer.removeAttribute(document.body, 'class');
    this.renderer.addClass(document.body, 'pageNotFound');
  }

  ngOnInit() {}
}
