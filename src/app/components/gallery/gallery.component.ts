import { Component, OnInit, OnDestroy } from '@angular/core';
import { Travel } from '../../services/models/travel.model';
import { Subscription } from 'rxjs';
import { TravelService } from '../../services/travel.service';

@Component({
  selector: 'app-gallery',
  templateUrl: './gallery.component.html',
  styleUrls: ['./gallery.component.css']
})
export class GalleryComponent implements OnInit, OnDestroy {
  travels: Travel[];
  private subscription: Subscription;

  constructor(private travelService: TravelService) {}

  ngOnInit() {
    this.subscription = this.travelService.getTravels().subscribe(result => {
      this.travels = result;
    });
  }

  getNumberOfImages(travel: Travel): string {
    if (travel.images) {
      const size = travel.images.length;
      switch (true) {
        case size === 1:
          return size + ' zdjęcie';
        case size > 1 && size < 5:
          return size + ' zdjęcia';
        default:
          return size + ' zdjęć';
      }
    } else {
      return '0 zdjęć';
    }
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
