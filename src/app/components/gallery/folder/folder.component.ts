import { Component, OnInit } from '@angular/core';
import { ImageService } from '../../../services/image.service';
import { Travel } from '../../../services/models/travel.model';
import { AlertService } from '../../../services/alert.service';
import { AuthService } from '../../../services/auth.service';
import { TravelService } from '../../../services/travel.service';
import { MappingService } from '../../../services/mapping.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-folder',
  templateUrl: './folder.component.html',
  styleUrls: ['./folder.component.css']
})
export class FolderComponent implements OnInit {
  travelID: string;
  travel: Travel;
  selectedImage: File;
  imageForm: FormGroup;
  clickedSubmitForm = false;

  constructor(
    private imageService: ImageService,
    private travelService: TravelService,
    private alertService: AlertService,
    private authService: AuthService,
    private mappingService: MappingService,
    private route: ActivatedRoute,
    private fb: FormBuilder
  ) {
    this.imageForm = fb.group({
      image: ['', Validators.required]
    });
  }

  ngOnInit() {
    this.travelID = this.route.snapshot.params['id'];
    this.travelService.getTravel(this.travelID).subscribe(
      result => {
        if (result.travel) {
          this.travel = this.mappingService.mapTravelFromBackend(
            result.travel[0]
          );
        } else {
          this.travel = result;
        }
      },
      err => {
        if (err.status === 401) {
          this.authService.goToLoginPage();
          this.alertService.warn(err.error.message, true);
        }
        this.alertService.error(err.error.message);
      }
    );
  }

  onFileSelected(event) {
    console.log(event);
    this.selectedImage = event.target.files[0];
  }

  uploadImage() {
    if (this.imageForm.valid && this.selectedImage) {
      this.imageService.addImage(this.travelID, this.selectedImage).subscribe(
        result => {
          this.selectedImage = null;
          this.alertService.success(result.message);
          const image = result.file;
          this.travel.images.push(
            this.mappingService.mapImageFromBackend(image)
          );
          console.log(result);
        },
        error => {
          if (error.status === 401) {
            this.alertService.warn(error.error.message, true);
            this.authService.goToLoginPage();
          } else {
            this.alertService.error(error.error.message);
          }
          console.log(error);
        }
      );
    } else {
      this.clickedSubmitForm = true;
    }
  }

  removeImage(imageName: string) {
    this.imageService.removeImage(this.travelID, imageName).subscribe(
      result => {
        this.travel.images.forEach((image, i) => {
          if (image.name === imageName) {
            this.travel.images.splice(i, 1);
          }
        });
        this.alertService.success(result.message);
      },
      error => {
        if (error.status === 401) {
          this.alertService.warn(error.error.message, true);
          this.authService.goToLoginPage();
        } else {
          this.alertService.error(error.error.message);
        }
        console.log(error);
      }
    );
  }
}
