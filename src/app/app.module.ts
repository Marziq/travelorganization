import { ImageService } from './services/image.service';
import { AlertService } from './services/alert.service';
import { AlertsComponent } from './components/alerts/alerts.component';
import { SharedService } from './services/shared.service';
import { UserResolve } from './user.resolve';
import { AuthGuard } from './auth.guard';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';
import { NgbModule, NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { AccordionModule } from 'primeng/accordion';
import { CalendarModule } from 'primeng/calendar';
import { AgmCoreModule } from '@agm/core';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatToolbarModule } from '@angular/material/toolbar';
import { ChartsModule } from 'ng2-charts';

import { AppRoutingModule } from './app-routing.module';
import { TravelsModule } from './components/travels/travels.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { AccommodationComponent } from './components/accommodation/accommodation.component';
import { ExpensesComponent } from './components/expenses/expenses.component';
import { GalleryComponent } from './components/gallery/gallery.component';

import { TravelService } from './services/travel.service';

import { AngularSvgIconModule } from 'angular-svg-icon';

import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { ModalAlertComponent } from './components/modal-alert/modal-alert.component';
import { SideNavbarComponent } from './components/side-navbar/side-navbar.component';
import { MappingService } from './services/mapping.service';
import { LoginComponent } from './components/login/login.component';
import { RegistrationComponent } from './components/registration/registration.component';
import { SortModule } from './components/pipes/sort.module';
import { FilterModule } from './components/pipes/filter.module';
import { FolderComponent } from './components/gallery/folder/folder.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AccommodationComponent,
    ExpensesComponent,
    GalleryComponent,
    PageNotFoundComponent,
    ModalAlertComponent,
    SideNavbarComponent,
    LoginComponent,
    RegistrationComponent,
    AlertsComponent,
    FolderComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    TravelsModule,
    ReactiveFormsModule,
    HttpModule,
    HttpClientModule,
    NgbModule.forRoot(),
    AppRoutingModule,
    AccordionModule,
    CalendarModule,
    MatSidenavModule,
    MatToolbarModule,
    NgbAlertModule,
    AngularSvgIconModule,
    SortModule,
    FilterModule,
    ChartsModule
  ],
  providers: [
    TravelService,
    MappingService,
    SharedService,
    AlertService,
    AuthGuard,
    UserResolve,
    ImageService
  ],
  bootstrap: [AppComponent],
  entryComponents: [ModalAlertComponent]
})
export class AppModule {}
