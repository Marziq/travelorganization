import { Injectable } from '@angular/core';
import { User } from './models/user.model';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Router, NavigationExtras } from '@angular/router';

interface IsLoggedIn {
  status: boolean;
}

interface Status {
  message: string;
}

interface UserData {
  user: User;
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private loggedInStatus = false;
  private user = null;

  constructor(private httpClient: HttpClient, private router: Router) {}

  get isLoggedIn(): boolean {
    return (
      this.loggedInStatus || 'true' == sessionStorage.getItem('loggedInStatus')
    );
  }

  setLoggedIn(value: boolean) {
    this.loggedInStatus = value;
    sessionStorage.setItem('loggedInStatus', value.toString());
  }

  setUser(user: User) {
    sessionStorage.setItem('firstName', user.firstName);
    sessionStorage.setItem('lastName', user.lastName);
    sessionStorage.setItem('email', user.email);
  }

  removeUser() {
    sessionStorage.removeItem('email');
    sessionStorage.removeItem('firstName');
    sessionStorage.removeItem('lastName');
    this.setLoggedIn(false);
  }

  getUserFirstName() {
    return sessionStorage.getItem('firstName');
  }
  getUserLastName() {
    return sessionStorage.getItem('lastName');
  }
  getUserEmail() {
    return sessionStorage.getItem('email');
  }

  getUser(): User {
    return new User(
      this.getUserEmail(),
      this.getUserFirstName(),
      this.getUserLastName()
    );
  }

  getUserData() {
    return this.httpClient.get<UserData>('/api/user');
  }

  login(user: User) {
    return this.httpClient.post<Status>('/api/user/login', user);
  }

  logout() {
    return this.httpClient.get<IsLoggedIn>('/api/user/logout');
  }

  isLoggedInUser(): Observable<IsLoggedIn> {
    return this.httpClient.get<IsLoggedIn>('/api/user/isloggedin');
  }

  registration(user: User) {
    return this.httpClient.post<Status>('/api/user/registration', user);
  }

  goToLoginPage() {
    this.router.navigate(['login']);
  }
}
