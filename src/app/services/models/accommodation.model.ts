import { Place } from './place.model';
export class Accommodation {
  constructor(
    public dateFrom: Date,
    public dateTo: Date,
    public place: Place,
    public price: number = 0
  ) {}
}
