import { Accommodation } from './accommodation.model';
import { ThingToPack } from './thing-to-pack.model';
import { Transport } from './transport.model';
import { Food } from './food.model';
import { Attraction } from './attraction.model';
import { Place } from './place.model';
import { Image } from './image.model';

export class Travel {
  static idSeq = 1;

  private _mongoID: string;
  private _id: number;
  private _title: string;
  private _dateFrom: Date;
  private _dateTo: Date;
  private _place: Place;
  private _description: string;
  private _price: number;
  private _thingsToPack: Array<ThingToPack>;
  private _transport: Array<Transport>;
  private _accommodations: Array<Accommodation>;
  private _food: Array<Food>;
  private _attractions: Array<Attraction>;
  private _images: Array<Image>;

  static create(
    id: string,
    title: string,
    dateFrom: Date,
    dateTo: Date,
    place: Place,
    description: string = '',
    price: number = 0,
    thingsToPack: Array<ThingToPack> = [],
    transport: Array<Transport> = [],
    accommodations: Array<Accommodation> = [],
    food: Array<Food> = [],
    attractions: Array<Attraction> = [],
    images: Array<Image> = []
  ): Travel {
    const travel = new Travel();
    travel.id = Travel.idSeq++;
    travel._mongoID = id;
    travel.title = title;
    travel.dateFrom = dateFrom;
    travel.dateTo = dateTo;
    travel.place = place;
    travel.description = description;
    travel.price = price;
    travel.thingsToPack = thingsToPack;
    travel.transport = transport;
    travel.accommodations = accommodations;
    travel.food = food;
    travel.attractions = attractions;
    travel.images = images;
    return travel;
  }

  get id(): number {
    return this._id;
  }

  set id(value: number) {
    this._id = value;
  }

  get mongoID(): string {
    return this._mongoID;
  }

  set mongoID(value: string) {
    this._mongoID = value;
  }

  get title(): string {
    return this._title;
  }

  set title(value: string) {
    this._title = value;
  }

  get dateFrom(): Date {
    return this._dateFrom;
  }

  set dateFrom(value: Date) {
    this._dateFrom = value;
  }
  get dateTo(): Date {
    return this._dateTo;
  }

  set dateTo(value: Date) {
    this._dateTo = value;
  }
  get place(): Place {
    return this._place;
  }

  set place(value: Place) {
    this._place = value;
  }
  get description(): string {
    return this._description;
  }

  set description(value: string) {
    this._description = value;
  }
  get price(): number {
    return this._price;
  }

  set price(value: number) {
    this._price = value;
  }

  get thingsToPack(): Array<ThingToPack> {
    return this._thingsToPack;
  }

  set thingsToPack(value: Array<ThingToPack>) {
    this._thingsToPack = value;
  }
  get transport(): Array<Transport> {
    return this._transport;
  }

  set transport(value: Array<Transport>) {
    this._transport = value;
  }
  get accommodations(): Array<Accommodation> {
    return this._accommodations;
  }

  set accommodations(value: Array<Accommodation>) {
    this._accommodations = value;
  }
  get attractions(): Array<Attraction> {
    return this._attractions;
  }

  set attractions(value: Array<Attraction>) {
    this._attractions = value;
  }
  get food(): Array<Food> {
    return this._food;
  }

  set food(value: Array<Food>) {
    this._food = value;
  }

  get images(): Array<Image> {
    return this._images;
  }

  set images(value: Array<Image>) {
    this._images = value;
  }
}
