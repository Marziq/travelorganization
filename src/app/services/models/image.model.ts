export class Image {
  constructor(public _id: string, public name: string, public path: string) {}
}
