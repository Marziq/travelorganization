import { Place } from './place.model';

export class Attraction {
  constructor(
    public name: string,
    public date: Date = null,
    public price: number = 0,
    public place: Place = null
  ) {}
}
