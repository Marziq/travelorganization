export class Food {
  constructor(
    public name: string,
    public date: Date = null,
    public price: number = 0
  ) {}
}
