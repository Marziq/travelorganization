export class Place {
  constructor(
    public name: string,
    public address: string = '',
    public latitude: number = 0,
    public longitude: number = 0
  ) {}
}
