import { Place } from './place.model';

export class Transport {
  constructor(
    public type: string,
    public dateFrom: Date = null,
    public dateTo: Date = null,
    public placeFrom: Place,
    public placeTo: Place,
    public price: number = 0
  ) {}
}
