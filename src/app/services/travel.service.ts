import { MappingService } from './mapping.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Travel } from './models/travel.model';
import { Place } from './models/place.model';
import { Transport } from './models/transport.model';
import { ThingToPack } from './models/thing-to-pack.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Food } from './models/food.model';
import { Attraction } from './models/attraction.model';
import { Accommodation } from './models/accommodation.model';
import { AlertService } from './alert.service';
import { AuthService } from './auth.service';

interface TravelsData {
  travels: any;
}

interface TravelData {
  travel: any;
}

interface StatusAddTravel {
  message: string;
  travelID: string;
}

interface Status {
  message: string;
}

@Injectable()
export class TravelService {
  private _observableTravels: BehaviorSubject<Travel[]>;
  private travels: Travel[] = [];

  constructor(
    private httpClient: HttpClient,
    private mapping: MappingService,
    private alertService: AlertService,
    private authService: AuthService
  ) {
    this._observableTravels = <BehaviorSubject<Travel[]>>(
      new BehaviorSubject([])
    );
  }

  getTravels(): Observable<Travel[]> {
    if (this._observableTravels.getValue().length === 0) {
      this.getTravelsFromBackend().subscribe(
        result => {
          this.setTravels(result.travels);
        },
        err => {
          if (err.status === 401) {
            this.alertService.warn(err.error.message, true);
            this.authService.goToLoginPage();
          } else {
            this.alertService.error(err.error.message);
          }
        }
      );
    }
    return this._observableTravels.asObservable();
  }

  getDataForChart() {
    return this.getTravelsFromBackend().pipe(
      map(items => {
        return items.travels.map(item => {
          const accommodation = this.totalSum(item.accommodations, true);
          const transport = this.totalSum(item.transport);
          const food = this.totalSum(item.food);
          const attraction = this.totalSum(item.attractions);
          return {
            dateFrom: new Date(item.dateFrom),
            dateTo: new Date(item.dateTo),
            totalPrice: this.getTotalPrice(
              item,
              accommodation,
              transport,
              food,
              attraction
            ),
            accommodationPrice: accommodation,
            transportPrice: transport,
            foodPrice: food,
            attractionPrice: attraction
          };
        });
      })
    );
  }

  totalSum(items, isAccomodation = false): number {
    let total = 0;
    if (items && items.length !== 0) {
      total = items.reduce((runningValue: number, item: any) => {
        return (
          runningValue +
          (item.price > 0
            ? item.price *
              (isAccomodation
                ? this.getDaysOfAccomodation(
                    new Date(item.dateFrom),
                    new Date(item.dateTo)
                  )
                : 1)
            : 0)
        );
      }, 0);
    }
    return total;
  }

  getTotalPrice(
    travel,
    accommodationCost,
    transportCost,
    foodCost,
    attractionCost
  ) {
    const accommodation = accommodationCost
      ? accommodationCost
      : this.totalSum(travel.accommodations, true);
    const transport = transportCost
      ? transportCost
      : this.totalSum(travel.transport);
    const food = foodCost ? foodCost : this.totalSum(travel.food);
    const attraction = attractionCost
      ? attractionCost
      : this.totalSum(travel.attractions);

    return (
      (!travel.price ? 0 : travel.price) +
      accommodation +
      transport +
      food +
      attraction
    );
  }

  getDaysOfAccomodation(dateFrom: Date, dateTo: Date) {
    const milisecPerDay = 24 * 60 * 60 * 1000;
    let days = 1;
    if (dateFrom !== null && dateTo.getTime() !== dateFrom.getTime()) {
      days = (dateTo.getTime() - dateFrom.getTime()) / milisecPerDay;
    }
    return days;
  }

  getTravel(travelID: string): Observable<any> {
    if (this._observableTravels.getValue().length === 0) {
      return this.getTravelByID(travelID);
    } else {
      return this._observableTravels.pipe(
        map(items => items.find(travel => travel.mongoID === travelID))
      );
    }
  }

  getTravelByID(travelID: string) {
    return this.httpClient.get<TravelData>('/api/travels/' + travelID);
  }

  addTravel(travel: Travel) {
    const travelObj = this.mapping.getTravelToBackend(travel);
    return this.httpClient.post<StatusAddTravel>('/api/travels', travelObj);
  }

  pushTravelToList(travel) {
    console.log('Dodano: ' + travel);
    this.travels.push(travel);
    this._observableTravels.next(this.travels);
  }

  removeTravelByID(travelID: string) {
    this.travels.forEach((travel, i) => {
      if (travel.mongoID === travelID) {
        this.travels.splice(i, 1);
      }
    });
    this._observableTravels.next(this.travels);
  }

  removeTravel(travelID: string) {
    return this.httpClient.delete<Status>('/api/travels/' + travelID);
  }

  updateTravelInfo(travelID: string, travel: Travel) {
    return this.httpClient.put<Status>('/api/travels/' + travelID, {
      travel: travel
    });
  }

  updateTravelFood(travelID: string, food: Food[]) {
    return this.httpClient.put<Status>('/api/travels/' + travelID + '/food', {
      food: food
    });
  }

  updateTravelThingsToPack(travelID: string, thingsToPack: ThingToPack[]) {
    return this.httpClient.put<Status>(
      '/api/travels/' + travelID + '/thingstopack',
      {
        thingsToPack: thingsToPack
      }
    );
  }

  updateTravelAttractions(travelID: string, attractions: Attraction[]) {
    return this.httpClient.put<Status>(
      '/api/travels/' + travelID + '/attractions',
      {
        attractions: attractions
      }
    );
  }

  updateTravelTransport(travelID: string, transport: Transport[]) {
    return this.httpClient.put<Status>(
      '/api/travels/' + travelID + '/transport',
      {
        transport: transport
      }
    );
  }

  updateTravelAccommodations(
    travelID: string,
    accommodations: Accommodation[]
  ) {
    return this.httpClient.put<Status>(
      '/api/travels/' + travelID + '/accommodations',
      {
        accommodations: accommodations
      }
    );
  }

  setTravels(result: any) {
    this.travels = this.mapping.getTravelsFromBack(result);
    this._observableTravels.next(this.travels);
  }

  clearTravels() {
    this.travels = [];
    this._observableTravels.next(this.travels);
  }

  getTravelsFromBackend() {
    return this.httpClient.get<TravelsData>('/api/travels');
  }
}
