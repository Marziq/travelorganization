import { Injectable } from '@angular/core';
import { User } from './models/user.model';
import { Subject } from 'rxjs';

@Injectable()
export class SharedService {
  // Observable string sources
  private emitChangeUser = new Subject<User>();
  // Observable string streams
  changeEmitted$ = this.emitChangeUser.asObservable();
  // Service message commands
  emitChange(userChanged: User) {
    this.emitChangeUser.next(userChanged);
  }

  getCalendarPL(): any {
    return {
      firstDayOfWeek: 1,
      dayNames: [
        'Niedziela',
        'Poniedziałek',
        'Wtorek',
        'Środa',
        'Czwartek',
        'Piątek',
        'Sobota'
      ],
      dayNamesShort: ['Nd', 'Pon', 'Wt', 'Śr', 'Czw', 'Pt', 'Sob'],
      dayNamesMin: ['Nd', 'Pn', 'Wt', 'Śr', 'Czw', 'Pt', 'Sb'],
      monthNames: [
        'Styczeń',
        'Luty',
        'Marzec',
        'Kwiecień',
        'Maj',
        'Czerwiec',
        'Lipiec',
        'Sieprień',
        'Wrzesień',
        'Październik',
        'Listopad',
        'Grudzień'
      ],
      monthNamesShort: [
        'Sty',
        'Lut',
        'Mar',
        'Kwi',
        'Maj',
        'Cze',
        'Lip',
        'Sie',
        'Wrz',
        'Paź',
        'Lis',
        'Gru'
      ],
      today: 'Dzisiaj',
      clear: 'Wyczyść'
    };
  }
}
