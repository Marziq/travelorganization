import { Injectable } from '@angular/core';
import { Travel } from './models/travel.model';
import { Place } from './models/place.model';
import { Transport } from './models/transport.model';
import { ThingToPack } from './models/thing-to-pack.model';
import { Accommodation } from './models/accommodation.model';
import { Food } from './models/food.model';
import { Attraction } from './models/attraction.model';
import { map } from 'rxjs/operators';
import { Image } from './models/image.model';

@Injectable()
export class MappingService {
  constructor() {}

  getTravelsFromBack(travelsArray): Travel[] {
    return travelsArray.map(t => this.mapTravelFromBackend(t));
  }

  // FROM BACKEND

  mapTravelFromBackend(travel): Travel {
    const newTravel = Travel.create(
      travel._id,
      travel.title,
      new Date(travel.dateFrom),
      new Date(travel.dateTo),
      !travel.place ? null : new Place(travel.place.name, travel.place.address),
      travel.description,
      travel.price,
      this.getThingsToPackFromBackend(travel.thingsToPack),
      this.getTransportsFromBackend(travel.transport),
      this.getAccomodationsFromBackend(travel.accommodations),
      this.getFoodFromBackend(travel.food),
      this.getAttractionsFromBackend(travel.attractions),
      this.getImagesFromBackend(travel.images)
    );
    return newTravel;
  }

  public getThingsToPackFromBackend(thingsToPack: any[]): ThingToPack[] {
    let listOfThingsToPack: ThingToPack[] = [];
    if (!this.isUndefined(thingsToPack)) {
      listOfThingsToPack = thingsToPack.map(
        t => new ThingToPack(t.name, !t.taken ? false : t.taken)
      );
    }
    return listOfThingsToPack;
  }

  public getTransportsFromBackend(transport: any[]): Transport[] {
    let listOfTransport: Transport[] = [];
    if (!this.isUndefined(transport)) {
      listOfTransport = transport.map(
        t =>
          new Transport(
            t.type,
            !t.dateFrom ? null : new Date(t.dateFrom),
            !t.dateTo ? null : new Date(t.dateTo),
            this.isUndefined(t.placeFrom) ? null : new Place(t.placeFrom.name),
            this.isUndefined(t.placeTo) ? null : new Place(t.placeTo.name),
            !t.price ? 0 : t.price
          )
      );
    }
    return listOfTransport;
  }

  public getAccomodationsFromBackend(accommodations: any[]): Accommodation[] {
    let listOfAccommodations: Accommodation[] = [];
    if (!this.isUndefined(accommodations)) {
      listOfAccommodations = accommodations.map(
        a =>
          new Accommodation(
            !a.dateFrom ? null : new Date(a.dateFrom),
            !a.dateTo ? null : new Date(a.dateTo),
            this.isUndefined(a.place)
              ? null
              : new Place(a.place.name, a.place.address),
            !a.price ? 0 : a.price
          )
      );
    }
    return listOfAccommodations;
  }

  public getFoodFromBackend(food: any[]): Food[] {
    let listOfFood: Food[] = [];
    if (!this.isUndefined(food)) {
      listOfFood = food.map(
        f =>
          new Food(
            f.name,
            !f.date ? null : new Date(f.date),
            !f.price ? 0 : f.price
          )
      );
    }
    return listOfFood;
  }

  public getAttractionsFromBackend(attractions: any[]): Attraction[] {
    let listOfAttractions: Attraction[] = [];
    if (!this.isUndefined(attractions)) {
      listOfAttractions = attractions.map(
        a =>
          new Attraction(
            a.name,
            !a.date ? null : new Date(a.date),
            !a.price ? 0 : a.price,
            this.isUndefined(a.place)
              ? null
              : new Place(a.place.name, a.place.address)
          )
      );
    }
    return listOfAttractions;
  }

  public getImagesFromBackend(images: any[]): Image[] {
    let listOfImages: Image[] = [];
    if (!this.isUndefined(images)) {
      listOfImages = images.map(i => this.mapImageFromBackend(i));
    }
    return listOfImages;
  }

  public mapImageFromBackend(image: any): Image {
    const imageName = image.name ? image.name : image.filename;
    return new Image(image._id, imageName, '/api/images/' + imageName);
  }

  // TO BACKEND

  getTravelToBackend(travel) {
    return {
      title: travel.title,
      dateFrom: this.isUndefined(travel.dateFrom)
        ? null
        : travel.dateFrom.toISOString(),
      dateTo: this.isUndefined(travel.dateTo)
        ? null
        : travel.dateTo.toISOString(),
      place: { name: travel.place.name, address: travel.place.address },
      description: travel.description,
      price: travel.price,
      thingsToPack: this.mapThingsToPackToBackEnd(travel.thingsToPack),
      transport: this.mapTransportToBackEnd(travel.transport),
      accommodations: this.mapAccommodationsToBackEnd(travel.accommodations),
      attractions: this.mapAttracctionsToBackEnd(travel.attractions),
      food: this.mapFoodToBackEnd(travel.food),
      images: this.mapImagesToBackEnd(travel.images)
    };
  }

  mapThingsToPackToBackEnd(thingsToPack: any) {
    return this.isUndefined(thingsToPack)
      ? []
      : thingsToPack.map(t => ({
          name: t.name,
          taken: t.taken
        }));
  }

  mapTransportToBackEnd(transport: any) {
    return this.isUndefined(transport)
      ? []
      : transport.map(t => ({
          type: t.type,
          dateFrom: this.isUndefined(t.dateFrom)
            ? null
            : t.dateFrom.toISOString(),
          dateTo: this.isUndefined(t.dateTo) ? null : t.dateTo.toISOString(),
          placeFrom: {
            name: t.placeFrom.name,
            address: t.placeFrom.address
          },
          placeTo: { name: t.placeTo.name, address: t.placeTo.address },
          price: this.isUndefined(t.price) || t.price <= 0 ? 0 : t.price
        }));
  }

  mapAccommodationsToBackEnd(accommodations: any) {
    return this.isUndefined(accommodations)
      ? []
      : accommodations.map(a => ({
          dateFrom: this.isUndefined(a.dateFrom)
            ? null
            : a.dateFrom.toISOString(),
          dateTo: this.isUndefined(a.dateTo) ? null : a.dateTo.toISOString(),
          place: { name: a.place.name, address: a.place.address },
          price: this.isUndefined(a.price) || a.price <= 0 ? 0 : a.price
        }));
  }

  mapAttracctionsToBackEnd(attractions: any) {
    return this.isUndefined(attractions)
      ? []
      : attractions.map(a => ({
          name: a.name,
          date: this.isUndefined(a.date) ? null : a.date.toISOString(),
          place: { name: a.place.name, address: a.place.address },
          price: this.isUndefined(a.price) || a.price <= 0 ? 0 : a.price
        }));
  }

  mapFoodToBackEnd(food: any) {
    return this.isUndefined(food)
      ? []
      : food.map(f => ({
          name: f.name,
          date: this.isUndefined(f.date) ? null : f.date.toISOString(),
          price: this.isUndefined(f.price) || f.price <= 0 ? 0 : f.price
        }));
  }

  mapImagesToBackEnd(images: any) {
    return this.isUndefined(images)
      ? []
      : images.map(i => ({
          _id: i._id,
          name: i.name
        }));
  }

  isUndefined(obj: any): boolean {
    return !obj || obj.length === 0;
  }
}
