import { MappingService } from './mapping.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Travel } from './models/travel.model';
import { Place } from './models/place.model';
import { Transport } from './models/transport.model';
import { ThingToPack } from './models/thing-to-pack.model';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Food } from './models/food.model';
import { Attraction } from './models/attraction.model';
import { Accommodation } from './models/accommodation.model';
import { AlertService } from './alert.service';
import { AuthService } from './auth.service';

interface Status {
  message: string;
  file: any;
}

@Injectable()
export class ImageService {
  private _observableTravels: BehaviorSubject<Travel[]>;

  constructor(
    private httpClient: HttpClient,
    private mapping: MappingService,
    private alertService: AlertService,
    private authService: AuthService
  ) {
    this._observableTravels = <BehaviorSubject<Travel[]>>(
      new BehaviorSubject([])
    );
  }

  getAllImages(): any {
    return this.httpClient.get('/api/images');
  }

  getImages(travelID: string): any {
    return this.httpClient.get('/api/images/travel/' + travelID);
  }

  removeImage(travelID: string, imageName: string): any {
    return this.httpClient.delete(
      '/api/images/' + imageName + '/travel/' + travelID
    );
  }

  addImage(travelID: string, form: any) {
    const formData = new FormData();
    formData.append('image', form);
    return this.httpClient.post<Status>(
      '/api/images/travel/' + travelID,
      formData
    );
  }
}
